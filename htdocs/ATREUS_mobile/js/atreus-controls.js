/**	ATREUS control library
    @author: http://atreus.ftw.at
	@year:	2014
	
	Functions for remote controls.
	Supported events: (depending on browser version):
		- touch events: button, touchpad with scroll/swipe/zoom
		- keyboard events
		- motion sensors: orientation,accelerometer, rotation
		- media capture: minivideo 
		- geolocation
		
	Other functions:	
		- fullscreen mode
		- create a json message for the server
		- add/remove events

 **/

var ATREUS_INTERACTION_BUTTON = 0;
var ATREUS_INTERACTION_TOUCH = 1;
var ATREUS_INTERACTION_KEY = 2;
var ATREUS_INTERACTION_ORIENT = 3;
var ATREUS_INTERACTION_ACC = 4;
var ATREUS_INTERACTION_VIDEO = 5;
var ATREUS_INTERACTION_AUDIO = 6;
var ATREUS_INTERACTION_GEO = 7;

//actions of button and touch
var ATREUS_UP = 0;
var ATREUS_DOWN = 1;
var ATREUS_MOVE = 2;
var ATREUS_LONGDOWN = 3;
var ATREUS_SWIPE_RIGHT = 4;
var ATREUS_SWIPE_LEFT = 5;
var ATREUS_SCROLL_UP = 6;
var ATREUS_SCROLL_DOWN = 7;
var ATREUS_ZOOM_IN = 8;
var ATREUS_ZOOM_OUT = 9;

//motion actions
var ATREUS_ACC = 0;
var ATREUS_ACC_GRAVITY = 1;
var ATREUS_ROTATION = 2;

var startPressTime, endPressTime, startDistance;
var startX, startY, lastX, lastY, endX, endY, timeStart, timeEnd;
var moved, zooming, touched;
var diff;
var startSending = false;

//minivideo
var startMx, startMy;
var screenMWidth;
var screenMHeight;
var screenMx;
var screenMy;

//UTILS	

function launchFullscreen(element) {
	//Fullscreen mode, works only on user action!!!
	if (!element.fullscreenElement && !element.mozFullScreenElement
			&& !element.webkitFullscreenElement && !element.msFullscreenElement) {
		if (element.requestFullscreen) {
			element.requestFullscreen();
		} else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if (element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		} else if (element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}

	}

}

function createControlMsg(intactType, action, elementId, x, y, z, sensorData) {
	var msgObj = {};
	var msgCoordObj = {};

	msgObj.interactionType = intactType;
	msgObj.action = action;
	msgObj.elementId = elementId;

	msgCoordObj.x = x;
	msgCoordObj.y = y;
	msgCoordObj.z = z;
	msgObj.coords = msgCoordObj;

	msgObj.sensorData = sensorData;

	var msg = JSON.stringify(msgObj);

	return msg;

}

//SET EVENTS
function addControlEvent(eventObj, eventType, eventFunction) {

	for (i = 0; i < eventObj.length; i++) {
		eventObj[i].addEventListener(eventType, eventFunction, false);
	}

}

function removeControlEvent(eventObj, eventType, eventFunction) {

	for (i = 0; i < eventObj.length; i++) {
		eventObj.removeEventListener(eventType, eventFunction, false);
	}

}

/** INTERACTION METHODS**/

//BUTTON 
function buttonPressed(event) {

	event.preventDefault();

	startPressTime = new Date().getTime();
	moved = false;

	var id = event.currentTarget.id;
	var x = event.changedTouches[0].pageX;
	var y = event.changedTouches[0].pageY;

	var sensorData = new Array();
	var attribute = event.changedTouches[0].target.getAttribute("src");
	sensorData[0] = attribute;

	var deviceMsg = createControlMsg(ATREUS_INTERACTION_BUTTON, ATREUS_DOWN,
			id, x, y, null, sensorData);

	//send to server
	sendData(deviceMsg);
}

function buttonReleased(event) {

	event.preventDefault();

	endPressTime = new Date().getTime();
	moved = false;

	//button released after longpressed action
	var action = null;
	var LONG_PRESSED_TIMEOUT = 750;
	if (!moved && (endPressTime - startPressTime) > LONG_PRESSED_TIMEOUT) {
		action = ATREUS_LONGDOWN;
	} else {
		action = ATREUS_UP;
	}

	var id = event.currentTarget.id;
	var x = event.changedTouches[0].pageX;
	var y = event.changedTouches[0].pageY;

	var sensorData = new Array();
	var attribute = event.changedTouches[0].target.getAttribute("src");
	sensorData[0] = attribute;

	var deviceMsg = createControlMsg(ATREUS_INTERACTION_BUTTON, action, id, x,
			y, null, sensorData);

	//send to server
	sendData(deviceMsg);
}

//TOUCH EVENTS
function touchMove(event) {

	event.preventDefault();
	var action = null;
	moved = true;
	var scaling;
	var id = event.currentTarget.id;

	/*var multiTouchAmount;
	for (i = 0; i < event.touches.length; i++) {

		var x = event.touches[i].pageX;
		var y = event.touches[i].pageY;
		multiTouchAmount = i + 1;

	}*/

	if (event.touches.length >= 2) {
		var x0 = event.touches[0].pageX;
		var x1 = event.touches[1].pageX;
		var y0 = event.touches[0].pageY;
		var y1 = event.touches[1].pageY;

		var dist = Math.sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1));

		diff = startDistance - dist;

		if (diff < 0) {
			action = ATREUS_ZOOM_IN;
			scaling = startDistance / dist;
		} else {
			action = ATREUS_ZOOM_OUT;
			scaling = dist / startDistance;
		}

		var sensorData = new Array();

		sensorData[0] = scaling;
		sensorData[1] = x0;
		sensorData[2] = y0;
		sensorData[3] = x1;
		sensorData[4] = y1;

		var deviceMsg = createControlMsg(ATREUS_INTERACTION_TOUCH, action, id,
				null, null, null, sensorData);

	} else {
		var sensorData = new Array();

		sensorData[0] = 1;
		sensorData[1] = window.innerWidth; //todo: fullscreen bzw. optimize toucharea size
		sensorData[2] = window.innerHeight;

		var diffX = event.touches[0].pageX - lastX;
		var diffY = event.touches[0].pageY - lastY;
	    lastX = event.touches[0].pageX;
	    lastY = event.touches[0].pageY;

		var deviceMsg = createControlMsg(ATREUS_INTERACTION_TOUCH, ATREUS_MOVE,
				id, diffX, diffY, null, sensorData);

	}
	//send to server
	sendData(deviceMsg);

}

function touchStart(event) {
	event.preventDefault();
	timeStart = new Date().getTime();
	moved = true;

	startX = event.touches[0].pageX;
	startY = event.touches[0].pageY;
    lastX = startX;
    lastY = startY;

	if (event.touches.length == 2) {
		zooming = true;

		startDistance = Math
				.sqrt((event.touches[0].pageX - event.touches[1].pageX)
						* (event.touches[0].pageX - event.touches[1].pageX)
						+ (event.touches[0].pageY - event.touches[1].pageY)
						* (event.touches[0].pageY - event.touches[1].pageY));

	} else {
		zooming = false;
	}

}

function touchEnd(event) {
	event.preventDefault();
	var action = null;
	timeEnd = new Date().getTime();
	moved = true;

	if (zooming) {
		zooming = false;
	}

	var timeDiff = timeEnd - timeStart;

	var id = event.currentTarget.id;
	endX = event.changedTouches[0].pageX;
	endY = event.changedTouches[0].pageY;
	var sensorData = new Array();

	var xDiff = startX - endX;
	var yDiff = startY - endY;
	sensorData[0] = xDiff;
	sensorData[1] = yDiff;

	//only one touch for swiping or scroll gesture
	if (event.touches.length == 0) {

		if (timeDiff > 200) {
			//normal move
			action = ATREUS_MOVE;
		} else {

			if (Math.abs(xDiff) > Math.abs(yDiff)) {
				if (xDiff > 0) { //left
					action = ATREUS_SWIPE_LEFT;
				} else { //right
					action = ATREUS_SWIPE_RIGHT;
				}

				var deviceMsg = createControlMsg(ATREUS_INTERACTION_TOUCH,
						action, id, endX, endY, null, sensorData);

			} else if (Math.abs(xDiff) < Math.abs(yDiff)) {
				//scroll 
				if (yDiff > 0) { //up
					action = ATREUS_SCROLL_UP;
				} else { //down
					action = ATREUS_SCROLL_DOWN;
				}

				var deviceMsg = createControlMsg(ATREUS_INTERACTION_TOUCH,
						action, id, endX, endY, null, sensorData);

			}
		}
	}

	//send to server
	sendData(deviceMsg);

}

//KEYBOARD
function keyboardPressed(event) {
	event.preventDefault();

	var keycode = (event.keyCode ? event.keyCode : event.which);

	var keyboardChar = String.fromCharCode(keycode);

	var sensorData = new Array();
	sensorData[0] = keycode;
	sensorData[1] = keyboardChar;

	var deviceMsg = createControlMsg(ATREUS_INTERACTION_KEY, null, null, null,
			null, null, sensorData);
	sendData(deviceMsg);
}

//MOTION SENSOR EVENTS: ORIENTATION & ACCELERATION
function startDeviceEvent() {
	if (startSending == false) {
		startSending = true;
	}

}
function stopDeviceEvent() {

	if (startSending == true) {
		startSending = false;
	}

}

function deviceOrientation(event) {
	event.preventDefault();

	var sensorData = new Array();
	sensorData[0] = touched;

	if (window.DeviceOrientationEvent && startSending == true) {
		var alpha = event.alpha;
		var beta = event.beta;
		var gamma = event.gamma;

		if (alpha != null || beta != null || gamma != null) {

			var deviceMsg = createControlMsg(ATREUS_INTERACTION_ORIENT, null,
					null, alpha, beta, gamma, sensorData);
			//send to server
			sendData(deviceMsg);
		}
	}

}

function deviceAcceleration(event) {
	event.preventDefault();

	if (window.DeviceMotionEvent && startSending == true) {

		var x = event.acceleration.x;
		var y = event.acceleration.y;
		var z = event.acceleration.z;

		var interval = event.interval;

		var sensorData = new Array();
		sensorData[0] = interval;

		if (x != null || y != null || z != null) {

			var deviceMsg = createControlMsg(ATREUS_INTERACTION_ACC,
					ATREUS_ACC, null, x, y, z, sensorData);
			//send to server
			sendData(deviceMsg);
		}
	}

}
function deviceAccelerationGravity(event) {
	event.preventDefault();

	if (window.DeviceMotionEvent && startSending == true) {

		var x = event.accelerationIncludingGravity.x;
		var y = event.accelerationIncludingGravity.y;
		var z = event.accelerationIncludingGravity.z;

		var interval = event.interval;

		var sensorData = new Array();
		sensorData[0] = interval;

		if (x != null || y != null || z != null) {

			var deviceMsg = createControlMsg(ATREUS_INTERACTION_ACC,
					ATREUS_ACC_GRAVITY, null, x, y, z, sensorData);
			//send to server
			sendData(deviceMsg);
		}
	}

}

function deviceRotation(event) {
	event.preventDefault();

	if (window.DeviceMotionEvent && startSending == true) {

		var alpha = event.rotationRate.alpha;
		var beta = event.rotationRate.beta;
		var gamma = event.rotationRate.gamma;

		var interval = event.interval;

		var sensorData = new Array();
		sensorData[0] = interval;

		if (alpha != null || beta != null || gamma != null) {

			var deviceMsg = createControlMsg(ATREUS_INTERACTION_ACC,
					ATREUS_ROTATION, null, alpha, beta, gamma, sensorData);
			//send to server
			sendData(deviceMsg);
		}
	}

}
//MINIVIDEO

function initMiniVideo(id) {

	var obj = document.getElementById(id);
	var touchID = "#" + id;
	obj.addEventListener('touchstart', function(event) {
		var touch = event.touches[0];
		startMx = Math
				.round(((screenMWidth + screenMx) * event.touches[0].pageX)
						/ ($(touchID).width()));
		startMy = Math
				.round(((screenMHeight + screenMy) * event.touches[0].pageY)
						/ ($(touchID).height()));

	}, false);

	obj.addEventListener('touchend', function(event) {
		var touch = event.changedTouches[0];
		var endMx = Math.round(((screenMWidth + screenMx) * touch.pageX)
				/ ($(touchID).width()));
		var endMy = Math.round(((screenMHeight + screenMy) * touch.pageY)
				/ ($(touchID).height()));

		var dx = Math.abs(startMx - endMx);
		var dy = Math.abs(startMy - endMy);

		// send button event if touchend was close to touchstart (simple tap)
		if (dx < 5 && dy < 5) {
			var intactType = ATREUS_INTERACTION_TOUCH;
			var action = ATREUS_DOWN;
			var id = touch.target.getAttribute("id");
			var x = endMx;
			var y = endMy;
			var attribute = touch.target.getAttribute("src");
			var sensorData = new Array();
			sensorData[0] = attribute;

			var msg = createControlMsg(intactType, action, id, x, y, null,
					sensorData);
			//send data to server
			sendData(msg);

		}

	}, false);

	obj.addEventListener('touchmove', function(event) {
		event.preventDefault();
		var touch = event.touches[0];

		var intactType = ATREUS_INTERACTION_TOUCH;
		var action = ATREUS_MOVE;
		var id = event.touches[0].target.getAttribute("id");

		var x = Math.round((screenMWidth * event.touches[0].pageX)
				/ ($(touchID).width()));
		var y = Math.round((screenMHeight * event.touches[0].pageY)
				/ ($(touchID).height()));

		var attribute = event.touches[0].target.getAttribute("src");
		var sensorData = new Array();
		sensorData[0] = attribute;

		var msg = createControlMsg(intactType, action, id, x, y, null,
				sensorData);
		//send data to server
		sendData(msg);

	}, false);

	var newURL = wsURL.split(":");

	var wsURLScreen = newURL[0] + ":" + newURL[1] + ":"
			+ (parseInt(newURL[2]) + 1);

	wss = new WebSocket(wsURLScreen);

	wss.onopen = function(event) {
		alert("Minivideo opened");
	}
	wss.onmessage = function(event) {
		var msg = event.data.toString();
		var imgarea = $('#' + id);
		imgarea.css("background-image", "url(data:image/jpg;base64," + msg
				+ ")");

	}

	wss.onclose = function(event) {

	}
	ws.onmessage = function(event) {
		var msg = event.data.toString();
		try {
			var obj = JSON.parse(msg);

			if (obj.hasOwnProperty('screenx') && obj.hasOwnProperty('screeny')
					&& obj.hasOwnProperty('screenwidth')
					&& obj.hasOwnProperty('screenheight')) {

				screenMx = obj.screenx;
				screenMy = obj.screeny;
				screenMWidth = obj.screenwidth;
				screenMHeight = obj.screenheight;
			}
		} catch (e) {
			console.error("Parsing error:", e);
		}

	}

}

//GEOLOCATION
function sendGeoLocation(continous) {

	if (navigator && navigator.geolocation) {

		if (continous == false) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var latitude = position.coords.latitude;
				var longitude = position.coords.longitude;
				var altitude = position.coords.altitude;

				var deviceMsg = createControlMsg(ATREUS_INTERACTION_GEO, null,
						null, latitude, longitude, altitude, null);
				//send to server
				sendData(deviceMsg);

			});
		} else {
			navigator.geolocation.watchPosition(function(position) {
				var latitude = position.coords.latitude;
				var longitude = position.coords.longitude;
				var altitude = position.coords.altitude;

				var deviceMsg = createControlMsg(ATREUS_INTERACTION_GEO, null,
						null, latitude, longitude, altitude, null);
				//send to server
				sendData(deviceMsg);

			});
		}

	} else {
		console.log('Geolocation is not supported');
	}

}
