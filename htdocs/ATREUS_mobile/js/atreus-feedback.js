/**	ATREUS feedback library
    @author: http://atreus.ftw.at
	@year:	2014
	
	Functions for smartphone feedback.
	Supported feedback events (depending on browser version):
		- vibration
		- sound
		- text
		
	To use this library "feedback = true" must be set in atreus_config.json!	

 **/

var feedbackObj;
var vibEvent = null;
var soundEvent = null;
var textEvent = null;
var mySound = null;
var context = null;

var ATREUS_FB_VIBRATION = 0;
var ATREUS_FB_AUDIO = 1;
var ATREUS_FB_TEXT = 2;

/*INIT EVENTS */
function addVibrationEventListener(eventFunction) {

	vibEvent = new CustomEvent('vibration', {
		bubbles : true,
		cancelable : true
	});

	document.addEventListener('vibration', eventFunction, false);

}

function addSoundEventListener(eventFunction) {

	soundEvent = new CustomEvent('sound', {
		bubbles : true,
		cancelable : true
	});

	document.addEventListener('sound', eventFunction, false);

}

/**
 setting an ID is optional
 */
function addTextEventListener(eventFunction, textareaID) {

	textEvent = new CustomEvent('text', {
		detail : {
			id : textareaID
		},
		bubbles : true,
		cancelable : true
	});

	document.addEventListener('text', eventFunction, false);

}

function receiveFeedback(serverData) {

	var objMsg = JSON.parse(serverData);

	feedbackObj = new Object();
	feedbackObj.param = objMsg.feedback_param;
	feedbackObj.broadcast = objMsg.feedback_broadcast;
	feedbackObj.type = objMsg.feedback_type;

	if (vibEvent != null) {

		if (feedbackObj.type == ATREUS_FB_VIBRATION) {

			document.dispatchEvent(vibEvent);
		}

	}

	if (soundEvent != null) {

		if (feedbackObj.type == ATREUS_FB_AUDIO) {

			document.dispatchEvent(soundEvent);
		}

	}

	if (textEvent != null) {

		if (feedbackObj.type == ATREUS_FB_TEXT) {

			document.dispatchEvent(textEvent);
		}

	}

}

/*EVENT FUNCTIONS */
function vibrationTest(event) {

	navigator.vibrate = navigator.vibrate || navigator.webkitVibrate
			|| navigator.mozVibrate || navigator.msVibrate;

	if (navigator.vibrate) {
		// cancel any existing vibrations
		navigator.vibrate(0);
		navigator.vibrate(feedbackObj.param);

	} else {
		//alert("No vibration supported!");	
	}

}

function soundTest(event) {

	var audio = document.getElementsByTagName("audio")[0];
	audio.src = feedbackObj.param[0];
	audio.volume = feedbackObj.param[1];
	audio.play();

}

function textTest(event) {

	var $textarea = $(event.detail.id);

	if ($textarea.val() != null) {

		$textarea.val($textarea.val() + feedbackObj.param + " | "
				+ feedbackObj.type + " | " + feedbackObj.broadcast + "\n");
		$textarea.animate({
			scrollTop : $textarea.height()
		}, 1000);
	}

}

function showText(event) {

	var $text = feedbackObj.param;

	alert($text);

}
