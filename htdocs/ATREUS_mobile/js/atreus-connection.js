/**	ATREUS connection library
    @author: http://atreus.ftw.at
	@year:	2014
	
	Functions for creating a websocket connection.
	wsURL and feedback is set by atreus_config.json
	Supported functions: 
		- create a websocket client
		- connect to websocket server
		- create a json server response message
		- send data to server
		
 **/

var ws;
var wsURL;
var responseObj;

$(document).ready(

function() {

	wsURL = "ws://192.168.0.18:8083";
	var feedback = true;

	ws = new WebSocket(wsURL);

	ws.onopen = function(event) {
		//alert("Connection opened!\n" + wsURL);

		responseObj = new Object();
		responseObj.responsecode = null;
		responseObj.message = null;
		responseObj.screenx = null;
		responseObj.screeny = null;
		responseObj.screenwidth = null;
		responseObj.screenheight = null;
		responseObj.feedback = null;

	}
	ws.onmessage = function(event) {
		var msg = event.data.toString();

		if (msg.indexOf("responsecode") > 0) {
			var objMsgResp = JSON.parse(event.data);

			responseObj.responsecode = objMsgResp.responsecode;
			responseObj.message = objMsgResp.message;
			responseObj.screenx = objMsgResp.screenx;
			responseObj.screeny = objMsgResp.screeny;
			responseObj.screenwidth = objMsgResp.screenwidth;
			responseObj.screenheight = objMsgResp.screenheight;
			responseObj.feedback = objMsgResp.feedback;

		}

		if (feedback && msg.indexOf("feedback_type") > 0) {

			try {
				receiveFeedback(event.data);
			} catch (e) {
				alert("atreus_feedback.js not attached!");
			}

		}

	}
	ws.onclose = function(event) {

		//alert("Connection closed!");
	}

});

function sendData(controlData) {

	if (ws.readyState == 1) {
		ws.send(controlData);
	}

}
