# Welcome to ATREUS!
![atreus_front.jpg](https://bitbucket.org/repo/7G6g7b/images/315175237-atreus_front.jpg)
ATREUS is a generic research framework for enabling smartphone-controlled applications for large displays. ATREUS does not require any native mobile applications but is based on state-of-the-art Web technologies.

The framework consists of two major parts: the platform and the mobile library. In the following, we provide a brief introduction to the framework and its demonstrators. The source code is provided under the [GNU General Public License](http://www.gnu.org/licenses/gpl.html). The project ATREUS was conducted by [FTW Telecommunications Research Center Vienna](http://www.ftw.at) and funded by [netidee](http://www.netidee.at/).

## Platform
The ATREUS platform is written in [Java](http://www.java.com) and needs to be executed on the computer hosting the large display. It contains an embedded [Jetty](http://www.eclipse.org/jetty/) server and affords convenient methods to manage the communication with mobile devices and react on incoming remote control commands. The central parameters for starting the ATREUS platform include

- ip: address (including http) of the platform, e.g. "http://1.2.3.4"
- port: port of the platform, e.g. 8080
- path: directory of files accessible from Web, e.g. "/htdocs" (default)
- remotecontrol: name of the HTML file to be served to devices, e.g. "touchpad.html"

These parameters are stored in JSON format in the configuration file data/atreus_config.json. To start the platform with the current configuration, simply use the *AtreusServer* class as demonstrated in the sample code below. For working with submitted remote control commands, set a custom *MobileActionListener*; for getting notified about connecting and disconnecting devices, a *MobileEventListener*.

```
    AtreusServer as = AtreusServer.getInstance();
    as.setActionListener(myMobileActionListener);
    as.setEventListener(myMobileEventListener);
    as.start(SettingsManager.getIP(), SettingsManager.getPort(), 
        AtreusConstants.HTML_PATH, SettingsManager.getRemoteControl());
```

>*StartAtreus.java* contains examples how to start the included sample remote controls and can be executed through the script *start* (.bat for Windows, .sh for Linux). If your not familiar with editing plain text configuration files, *StartAtreusGUI.java* (script *startgui*) provides a graphical user interface for setting these parameters and to start the platform. 

Additional parameters include

- maxuser: the maximal number of concurrent users (or devices), e.g. 4
- feedback: a boolean value (true/false) whether the platform may send feedback, e.g. true
- showqr: a boolean value (true/false whether a window with a QR code should be displayed, e.g. false

While many remote controls can be realized through this basic procedure, more advanced remote controls may require further configuration parameters as well as additional functionality at the platform. *Pointer*, *Mini Video* and *Smart Lens* are examples for such advanced interaction techniques which, for example, require the dimensions of the large screen specified through the following parameters:

- screenx: the left coordinate of the screen in pixels, e.g. 0
- screeny: the top coordinate of the screen in pixels, e.g. 0
- screenwidth: width of the screen in pixels, e.g. 1920
- screenheight: height of the screen in pixels, e.g. 1080

>Make sure that the ATREUS platform can be reached over the given IP and port and no firewall blocks access.

While a *MobileEventListener* is optional, the implementation of a *MobileActionListener* is required in order to make use of incoming remote control commands. The package at.ftw.atreus.io contains several exemplary listeners. For example, *GamepadListener.java* receives typical button presses and emulates corresponding key presses (mapped in a separate file called *gamepad.map*) through Java's [*Robot*](https://docs.oracle.com/javase/8/docs/api/java/awt/Robot.html) class. In a basic form, a MobileActionListener simply implements the *triggerAction* method which receives a *MobileData* element containing all necessary control parameters as follows:

```
    @Override
    public void triggerAction(MobileData mobiledata) {

        if (mobiledata != null) {
            if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_BUTTON) {

                int actionType = mobiledata.getAction();
                String elementId = mobiledata.getElementId();
                
                // command for movement to the left received?
                if ("player_left".equals(elementId)) {
	                if (actionType == AtreusConstants.ATREUS_DOWN) {
	                    robot.keyPress(KeyEvent.VK_LEFT);
	                } else if (actionType == AtreusConstants.ATREUS_UP) {
	                    robot.keyRelease(KeyEvent.VK_LEFT);
	                }                
                }
                // if ("player_right".equals(elementId)) ...              
            }
        }
    }
```

## Mobile Library
All ATREUS remote controls for mobile devices are HTML files which can exploit the rich features of HTML5. They are stored in the /htdocs folder and make use of the framework's mobile JavaScript library to transparently communicate with the platform for sending commands and (optionally) receiving feedback information. The mobile library comprises three JavaScript files with distinct functionality:

- atreus-connection.js: manages the connection to the ATREUS platform
- atreus-feedback.js: methods for reacting on platform-initiated feedback incl. exemplary implementations
- atreus-controls.js: tools for creating command messages, various methods used by demonstrators

These files and a recent version of the required jQuery library are included in the head of the HTML file as follows:

```
	<script type="text/javascript" charset="utf-8" src="ATREUS_mobile/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" charset="utf-8" src="ATREUS_mobile/js/atreus-connection.js"></script>
	<script type="text/javascript" charset="utf-8" src="ATREUS_mobile/js/atreus-feedback.js"></script>
	<script type="text/javascript" charset="utf-8" src="ATREUS_mobile/js/atreus-controls.js"></script>
```

A connection to the platform is established as soon as atreus-connection.js is loaded (through jQuery's $(document).ready() method). A basic example for sending a 'button down' event for a pressed button to the platform is the following JavaScript code:

```
	// create a remote command with these parameters:
	// interactionType: numerical constant (defined in atreus-controls.js) describing the type of interaction, e.g. ATREUS_INTERACTION_BUTTON or ATREUS_INTERACTION_TOUCH
	// Action:          numerical constant (defined in atreus-controls.js) describing the action, e.g. down/up for a button 
	// elementId:       string identifier for the control element
	// x, y, z:         optional coordinates, e.g. the concrete touch point of a button
	// sensordata:      optional array of additional data, typically from sensors
	var cmd = createControlMsg(ATREUS_INTERACTION_BUTTON, ATREUS_DOWN, "player_left", touchX, touchY, null, null);
	sendData(cmd);
```

In a corresponding listener on the platform side, the remote application can react on these commands and trigger actions with regard to the passed parameters.

### Demonstrators
ATREUS comes with several sample remote controls which demonstrate the features of the framework. All can be selected and started through the demo application *StartAtreusGUI*.
>These demos provide a good starting point for own experiments with the ATREUS framework. For example, start with a basic control such as gamepad.html and try to modify the corresponding *MobileActionListener* to change its behavior.

#### Driving Wheel
The driving wheel demo (drivingwheel.html) is an example for continuously transmitting sensor information (such as accelerometer data) from the mobile device to the ATREUS platform. There, for example, it can mapped to analog joystick movements or processed further. 

#### Feedback
Feedbackdemo.html contains a very basic example for receiving feedback information from the platform and trigger respective actions such as playing an audio sample. The corresponding application *FeedbackTest.java* sends out sample feedback information to any newly connected client.

#### Gamepads
ATREUS comes with several exemplary on-screen gamepads to control remote video games.

 * gamepad.html: a gamepad mimicking an old-school physical gamepad through images
 * gamepad_btn.html: a gamepad with directional buttons (used in the pointing test demo featuring vibration and audio feedback, see *StartDemo.java*)
 * gamepad_dpad.html: a gamepad with a directional pad
 * gamepad_joystick: a gamepad with a virtual joystick
 * gamepad_tilt: a gamepad exploiting the device til for movement controls

#### Mini Video
In the Mini Video demo (minivideo.html), the large screen is continously captured and transmitted to the connected mobile devices. There, it is scaled to the size of the mobile display; touches on this miniature version are sent to the platform and simulated as mouse clicks.

#### Pointer
The pointer demo (pointer.html) utilizes the smartphone similar to a laser pointer, i.e. the movements of the device are mapped to absolute cursor positions on the platform side. The button at the bottom triggers a left mouse click.

#### Smart Lens
The Smart Lens (smartlens.html) allows to select control elements on the remote screen by targeting and touching them through the camera viewfinder. When the user touches the mobile display, the current camera frame is sent to the platform. Using the computer vision library [BoofCV](http://boofcv.org) the frame is matched with the current screen content and - if successful - the touch coordinates mapped to corresponding screen coordinates.

#### Touchpad
The Touchpad (touchpad.html) resembles a traditional trackpad of a laptop computer, i.e. strokes on the mobile display result in corresponding movements of the remote mouse cursor. It supports multi-touch operations such as 'pinch-to-zoom', e.g. to zoom in and out on a maps shown on the large display.