/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

/**
 * The Class QRCodeCreator.
 * 
 * Simple helper for creating QR codes using zxing
 * 
 */
public class QRCodeCreator {

    /**
     * @param content
     *            String to be encoded in the created QR code
     * @param width
     *            width of the image to be created in pixels
     * @param height
     *            height of the image to be created in pixels
     * @param margin
     *            margin in pixels
     * @return QR code
     */
    public static BufferedImage create(String content, int width, int height,
            int margin) {

        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = null;

        Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(
                EncodeHintType.class);
        hints.put(EncodeHintType.MARGIN, margin);

        try {
            bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, width,
                    height, hints);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "gif", os);

            ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(os
                    .toByteArray()));

            os.close();
            is.close();

            return img;
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JFrame getQRCodeWindow(String content) {
        int width = 300;
        JFrame f = new JFrame("[ATREUS] Scan to connect!");
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        ImageIcon image = new ImageIcon(create(content, width, width, 2));
        JLabel qrlabel = new JLabel(image);
        f.setLayout(new BorderLayout());
        f.setResizable(false);
        f.setAlwaysOnTop(true);
        f.add(qrlabel, BorderLayout.CENTER);

        JLabel iplabel = new JLabel(content, SwingConstants.CENTER);
        iplabel.setOpaque(true);
        iplabel.setBackground(Color.WHITE);
        f.add(iplabel, BorderLayout.SOUTH);

        f.setSize(width + iplabel.getHeight(), width);
        f.pack();

        return f;
    }

}
