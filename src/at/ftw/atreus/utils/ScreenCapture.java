/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.utils;

import java.awt.AWTException;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;

/**
 * The Class ScreenCapture.
 * 
 * Utility class for capturing (scaled) screenshots encoded as JPGs
 * 
 */
public class ScreenCapture {

    // JPG compression, 0..1
    private final static float COMPRESSION = 0.3f;

    // offers screen capture capability
    private static Robot robot = null;

    // image writer and params for JPG compression
    private static ImageWriter writer = null;
    private static ImageWriteParam iwp = null;

    static {
        // try to create Robot instance
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // set up JPG writer for compressing screen shots
        Iterator<ImageWriter> iter = ImageIO
                .getImageWritersByFormatName("jpeg");
        writer = iter.next();
        iwp = writer.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwp.setCompressionQuality(COMPRESSION);
    }

    /**
     * @param screenrect
     *            screen area to be captured in pixels
     * @return JPG encoded screen shot
     */
    public static byte[] getJPGScreenshot(Rectangle screenrect) {
        return getJPGScreenshot(screenrect, 1f);
    }

    /**
     * @param screenrect
     *            screen area to be captured in pixels
     * @return screenshot as BufferedImage
     */
    public static BufferedImage getScreenshot(Rectangle screenrect) {
        return robot.createScreenCapture(screenrect);
    }

    /**
     * @param screenrect
     *            screen area to be captured in pixels
     * @param scaleFactor
     *            scale factor for resizing screen shot between 0 and 1
     * @return JPG encoded screenshot
     */
    public static byte[] getJPGScreenshot(Rectangle screenrect,
            float scaleFactor) {
        if (robot == null)
            return null;

        BufferedImage screenCapture = getScreenshot(screenrect);

        // create scaled version
        Image scaledImage = screenCapture.getScaledInstance(
                (int) (screenCapture.getWidth() * scaleFactor),
                (int) (screenCapture.getHeight() * scaleFactor),
                Image.SCALE_FAST);

        // and create a bufferedimage
        int width = scaledImage.getWidth(null);
        int height = scaledImage.getHeight(null);
        BufferedImage scaledBufferedImage = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics g = scaledBufferedImage.getGraphics();
        g.drawImage(scaledImage, 0, 0, null);
        g.dispose();

        return getJPGBytes(scaledBufferedImage);
    }

    private static byte[] getJPGBytes(BufferedImage image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            writer.setOutput(ImageIO.createImageOutputStream(baos));
            writer.write(null, new IIOImage(image, null, null), iwp);
            return baos.toByteArray();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
