/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.ImageIcon;

/**
 * The Class AtreusConstants.
 * 
 * Contains all settings for the ATREUS framework
 * 
 */
public class AtreusConstants {

    // Path, ConfigFile
    public final static String JS_FILE = "htdocs/ATREUS_mobile/js/atreus-connection.js";
    public final static String HTML_PATH = "./htdocs";
    public final static String JSON_CONFIG = "data" + File.separator
            + "atreus_config.json";
    public final static String REMOTE_CONTROL_PATH = "htdocs";

    // JSON VALUES
    public final static String CONFIG_PORT = "port";
    public final static String CONFIG_IP = "ip";
    public final static String CONFIG_SCREEN_X = "screenx";
    public final static String CONFIG_SCREEN_Y = "screeny";
    public final static String CONFIG_SCREEN_W = "screenwidth";
    public final static String CONFIG_SCREEN_H = "screenheight";
    public final static String CONFIG_REMOTECONTROL = "remotecontrol";
    public final static String CONFIG_MAXUSER = "maxuser";
    public final static String CONFIG_FEEDBACK = "feedback";
    public final static String CONFIG_QR = "showqr";

    // DEMO HTML FILES
    public final static String GAMEPAD = "gamepad.html";
    public final static String GAMEPAD_BTN = "gamepad_btn.html";
    public final static String GAMEPAD_DPAD = "gamepad_dpad.html";
    public final static String GAMEPAD_JOYSTICK = "gamepad_joystick.html";
    public final static String GAMEPAD_TILT = "gamepad_tilt.html";
    public final static String DRIVING_WHEEL = "drivingwheel.html";
    public final static String TOUCHPAD = "touchpad.html";
    public final static String MINIVIDEO = "minivideo.html";
    public final static String SMARTLENS = "smartlens.html";
    public final static String POINTER = "pointer.html";
    public final static String FB_DEMO = "feedbackdemo.html";

    // Interaction
    public final static int INTERACTION_BUTTON = 0;
    public final static int INTERACTION_TOUCH = 1;
    public final static int INTERACTION_TEXT = 2;
    public final static int INTERACTION_ORIENT = 3;
    public final static int INTERACTION_ACC = 4;
    public final static int INTERACTION_VIDEO = 5;
    public final static int INTERACTION_AUDIO = 6;
    public final static int INTERACTION_GEO = 7;

    public final static int ATREUS_UP = 0;
    public final static int ATREUS_DOWN = 1;
    public final static int ATREUS_MOVE = 2;
    public final static int ATREUS_LONGDOWN = 3;
    public final static int ATREUS_SWIPE_RIGHT = 4;
    public final static int ATREUS_SWIPE_LEFT = 5;
    public final static int ATREUS_SCROLL_UP = 6;
    public final static int ATREUS_SCROLL_DOWN = 7;
    public final static int ATREUS_ZOOM_IN = 8;
    public final static int ATREUS_ZOOM_OUT = 9;

    public final static String FEEDBACK_TYPE = "feedback_type";
    public final static String FEEDBACK_PARAM = "feedback_param";
    public final static String FEEDBACK_BROADCAST = "feedback_broadcast";

    public final static String FEEDBACK_TRUE = "var feedback = true;";
    public final static String FEEDBACK_FALSE = "var feedback = false;";
    public final static String JS_REGEX_WS = "wsURL = \".*\";";

    // feedback types
    public final static int FEEDBACK_VIBRATION = 0;
    public final static int FEEDBACK_SOUND = 1;
    public final static int FEEDBACK_TEXT = 2;

    // GUI
    public final static Dimension SETTINGFRAME_SIZE = new Dimension(370, 390);
    public final static Point SETTINGFRAME_POS = new Point(30, 30);
    public final static String FRAME_TITLE = "[ATREUS] Settings";
    public final static Color ATREUS_COLOR = Color.decode("#1e87b4");
    public final static ImageIcon ATREUS_LOGO = new ImageIcon("data"
            + File.separator + "atreus_logo.png");
    public final static Image ATREUS_ICON = Toolkit.getDefaultToolkit()
            .getImage("data" + File.separator + "atreus_icon.png");
    public final static String ATREUS_URL = "http://atreus.ftw.at";
}
