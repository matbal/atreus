/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.utils;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ftw.atreus.ui.ScreenRectangle;

/**
 * The Class AtreusUtils.
 * 
 * Helper class for ATREUS framework: Screen resolutions, remote controls,
 * websocket settings, URL and port validation
 * 
 */
public class AtreusUtils {

    private static final Logger log = LogManager.getLogger(AtreusUtils.class);

    public static ArrayList<ScreenRectangle> getScreenResolutions() {

        ArrayList<ScreenRectangle> result = new ArrayList<ScreenRectangle>();

        int i = 0;

        GraphicsEnvironment myGraphicEnv = GraphicsEnvironment
                .getLocalGraphicsEnvironment();

        GraphicsDevice[] allDevices = myGraphicEnv.getScreenDevices();
        for (GraphicsDevice myDevice : allDevices) {

            result.add(new ScreenRectangle(myDevice.getDefaultConfiguration()
                    .getBounds()));

            log.info("My displays: " + result.get(i));

            i++;
        }

        return result;
    }

    public static void setRemoteControls(ArrayList<String> myRCs) {

        File htdocFolder = new File(AtreusConstants.REMOTE_CONTROL_PATH);
        File[] fileList = htdocFolder.listFiles();

        if (fileList != null) {
            for (File myFile : fileList) {
                if (myFile.isFile()) {
                    myRCs.add(myFile.getName());
                }

            }

            log.info("Remote Control Files: " + myRCs);

        } else {
            log.error("No files available");
        }

    }

    /**
     * 
     * @param ip
     * @param port
     * @param jsFile
     * 
     *            set parameters in websocket file: ws - url: ip + port,
     *            feedback parameter
     */
    public static void initWebsocketSettings(String ip, int port, String jsFile) {

        String webSocketIP = null;
        String wsInfo;
        String feedbackValue = AtreusConstants.FEEDBACK_FALSE;
        String replacedFBText = AtreusConstants.FEEDBACK_FALSE;

        if (ip.startsWith("http")) {
            webSocketIP = ip.replace("http", "ws");
        } else if (ip.startsWith("https")) {
            webSocketIP = ip.replace("http", "wss");
        }

        if (webSocketIP.endsWith("/")) {
            wsInfo = webSocketIP.substring(0, webSocketIP.length() - 1);
        } else {
            wsInfo = webSocketIP;
        }

        Path path = Paths.get(jsFile);
        Charset charset = StandardCharsets.UTF_8;

        String content;
        String replacedTxt = null;
        try {
            content = new String(Files.readAllBytes(path), charset);

            if (SettingsManager.getRemoteControl().equals(
                    AtreusConstants.SMARTLENS)) {
                wsInfo = wsInfo + ":" + (port + 2); // port for smartlens.html
            } else {
                wsInfo = wsInfo + ":" + port;
            }

            log.info("currentIP: " + wsInfo);

            Matcher matcher = Pattern.compile(AtreusConstants.JS_REGEX_WS)
                    .matcher(content);

            while (matcher.find()) {
                String matchedTxt = matcher.group();

                replacedTxt = matchedTxt.split("\"")[1];

            }

            content = content.replaceFirst(replacedTxt, wsInfo);

            if (SettingsManager.isFeedback()) {
                replacedFBText = AtreusConstants.FEEDBACK_FALSE;
                feedbackValue = AtreusConstants.FEEDBACK_TRUE;
            } else {
                replacedFBText = AtreusConstants.FEEDBACK_TRUE;
                feedbackValue = AtreusConstants.FEEDBACK_FALSE;
            }

            content = content.replaceFirst(replacedFBText, feedbackValue);

            Files.write(path, content.getBytes(charset));

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (Throwable th) {
            log.info(th.getMessage());
        }

    }

    public static int validPort(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            log.error("Invalid port number");

        } catch (Throwable th) {
            th.printStackTrace();
            log.info(th.getMessage());
        }

        return 0;
    }

    public static String validURL(String url) {
        try {
            URL urlInput = new URL(url);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block

            log.error("Invalid url");
            return "";
        } catch (Throwable th) {
            th.printStackTrace();
        }

        return url;
    }

}
