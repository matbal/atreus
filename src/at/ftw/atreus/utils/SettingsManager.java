/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.utils;

import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * The Class SettingsManager.
 * 
 * Makes parameters from settings file accessible and save parameters in
 * atreus_config.json
 * 
 */
public class SettingsManager {

    private static int port;
    private static String ip;
    private static String url;
    private static String remotecontrol;
    private static int maxuser;
    private static int screenx;
    private static int screeny;
    private static int screenwidth;
    private static int screenheight;
    private static Rectangle screenrect;
    private static boolean feedback;
    private static boolean showqr;

    static {
        load();
    }

    public static boolean load() {
        try {
            LogManager.getLogger(SettingsManager.class).info(
                    "Reading settings from file...");

            byte[] configData = Files.readAllBytes(Paths
                    .get(AtreusConstants.JSON_CONFIG));
            ObjectMapper objMapper = new ObjectMapper();
            JsonNode rootConfigNode = objMapper.readTree(configData);

            // read specific data
            JsonNode currentNode = rootConfigNode
                    .path(AtreusConstants.CONFIG_PORT);
            port = currentNode.asInt();

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_IP);
            ip = currentNode.asText();

            if (!ip.startsWith("http")) {
                url = "http://" + ip + ":" + port;
            } else {
                url = ip + ":" + port;
            }

            currentNode = rootConfigNode
                    .path(AtreusConstants.CONFIG_REMOTECONTROL);
            remotecontrol = currentNode.asText();

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_MAXUSER);
            maxuser = currentNode.asInt();

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_SCREEN_X);
            screenx = currentNode.asInt();

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_SCREEN_Y);
            screeny = currentNode.asInt();

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_SCREEN_W);
            screenwidth = currentNode.asInt();

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_SCREEN_H);
            screenheight = currentNode.asInt();

            screenrect = new Rectangle(screenx, screeny, screenwidth,
                    screenheight);

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_FEEDBACK);
            feedback = currentNode.asBoolean();

            currentNode = rootConfigNode.path(AtreusConstants.CONFIG_QR);
            showqr = currentNode.asBoolean();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean save() {
        LogManager.getLogger(SettingsManager.class).info(
                "Saving settings to file...");
        try {
            byte[] configData = Files.readAllBytes(Paths
                    .get(AtreusConstants.JSON_CONFIG));
            ObjectMapper objMapper = new ObjectMapper();

            JsonNode rootConfigNode = objMapper.readTree(configData);

            // update data
            if (port != 0 && !ip.isEmpty() && !remotecontrol.isEmpty()
                    && screenrect != null) {
                ((ObjectNode) rootConfigNode).put(AtreusConstants.CONFIG_PORT,
                        port);
                ((ObjectNode) rootConfigNode)
                        .put(AtreusConstants.CONFIG_IP, ip);
                ((ObjectNode) rootConfigNode).put(
                        AtreusConstants.CONFIG_SCREEN_X, screenrect.x);
                ((ObjectNode) rootConfigNode).put(
                        AtreusConstants.CONFIG_SCREEN_Y, screenrect.y);
                ((ObjectNode) rootConfigNode).put(
                        AtreusConstants.CONFIG_SCREEN_W, screenrect.width);
                ((ObjectNode) rootConfigNode).put(
                        AtreusConstants.CONFIG_SCREEN_H, screenrect.height);
                ((ObjectNode) rootConfigNode).put(
                        AtreusConstants.CONFIG_REMOTECONTROL, remotecontrol);
                ((ObjectNode) rootConfigNode).put(
                        AtreusConstants.CONFIG_MAXUSER, maxuser);
                ((ObjectNode) rootConfigNode).put(
                        AtreusConstants.CONFIG_FEEDBACK, feedback);
                ((ObjectNode) rootConfigNode).put(AtreusConstants.CONFIG_QR,
                        showqr);
                objMapper.writeValue(new File(AtreusConstants.JSON_CONFIG),
                        rootConfigNode);
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static int getPort() {
        return port;
    }

    public static void setPort(int _port) {
        port = _port;
        if (!ip.startsWith("http")) {
            url = "http://" + ip + ":" + port;
        } else {
            url = ip + ":" + port;
        }
    }

    public static String getIP() {
        return ip;
    }

    public static void setIP(String _ip) {
        ip = _ip;
        if (!ip.startsWith("http")) {
            url = "http://" + ip + ":" + port;
        } else {
            url = ip + ":" + port;
        }
    }

    public static String getURL() {
        return url;
    }

    public static String getRemoteControl() {
        return remotecontrol;
    }

    public static void setRemoteControl(String _remotecontrol) {
        remotecontrol = _remotecontrol;
    }

    public static int getMaxUser() {
        return maxuser;
    }

    public static void setMaxUser(int _maxuser) {
        maxuser = _maxuser;
    }

    public static int getScreenX() {
        return screenx;
    }

    public static int getScreenY() {
        return screeny;
    }

    public static int getScreenWidth() {
        return screenwidth;
    }

    public static int getScreenHeight() {
        return screenheight;
    }

    public static Rectangle getScreenRect() {
        return screenrect;
    }

    public static void setScreenRect(Rectangle _screenrect) {
        screenrect = _screenrect;
        screenx = (int) screenrect.getX();
        screeny = (int) screenrect.getY();
        screenwidth = (int) screenrect.getWidth();
        screenheight = (int) screenrect.getHeight();
    }

    public static boolean isFeedback() {
        return feedback;
    }

    public static void setFeedback(boolean _feedback) {
        feedback = _feedback;
    }

    public static boolean isShowqr() {
        return showqr;
    }

    public static void setShowqr(boolean _showqr) {
        showqr = _showqr;
    }

}
