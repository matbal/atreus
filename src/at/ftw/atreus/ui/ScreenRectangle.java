/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.ui;

import java.awt.Rectangle;

/**
 * The Class ScreenRectangle.
 * 
 * Defines screen resolution and screen position used in GUI dropdown
 * 
 */
public class ScreenRectangle extends Rectangle {
    private static final long serialVersionUID = 831391159246668873L;

    public ScreenRectangle(Rectangle rect) {
        super(rect);
    }

    public ScreenRectangle(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    @Override
    public String toString() {
        return (int) getWidth() + "x" + (int) getHeight() + " (" + (int) getX()
                + ", " + (int) getY() + ")";
    }

}
