/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JPanel;

import at.ftw.atreus.utils.AtreusConstants;

/**
 * The Class SettingsPanel.
 * 
 * Defines the design for the ATREUS GUI
 */
public class SettingsPanel extends JPanel {

    private static final long serialVersionUID = -2892743849346526372L;

    public SettingsPanel() {

        this.setBackground(AtreusConstants.ATREUS_COLOR);
        this.setForeground(Color.WHITE);
        this.setFont(new Font("Arial", Font.PLAIN, 11));
        GridLayout settingGrid = new GridLayout(0, 2);
        this.setLayout(settingGrid);

    }

}
