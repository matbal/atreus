/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ftw.atreus.demos.DemoGamepadListener;
import at.ftw.atreus.io.DrivingWheelListener;
import at.ftw.atreus.io.GamepadListener;
import at.ftw.atreus.io.MinivideoListener;
import at.ftw.atreus.io.PointerListener;
import at.ftw.atreus.io.TouchpadListener;
import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.AtreusUtils;
import at.ftw.atreus.utils.QRCodeCreator;
import at.ftw.atreus.utils.SettingsManager;
import at.ftw.atreus.websocket.AtreusServer;
import at.ftw.atreus.websocket.PhotoReceiver;
import at.ftw.atreus.websocket.ScreenStreamer;

/**
 * The Class SettingsFrame.
 * 
 * Defines the frame and function for the ATREUS GUI
 * 
 */
public class SettingsFrame extends JFrame {

    private static final long serialVersionUID = -1325371860454989469L;
    private static final Logger log = LogManager.getLogger(SettingsFrame.class);
    private static SettingsFrame instance;
    private static SettingsPanel settingPanel;
    private static JFrame qrwindow;

    private static JTextField textPort = new JTextField();
    private static JTextField textAddress = new JTextField();
    private static JComboBox<ScreenRectangle> comboScreen = new JComboBox<>();
    private static JComboBox<String> comboRemoteControl = new JComboBox<>();
    private static JButton btnStop = new JButton("Stop");
    private static JButton btnStart = new JButton("Start");
    private static JSpinner spinnerUserAmount = new JSpinner();
    private static JCheckBox checkBoxFB = new JCheckBox();
    private static JCheckBox checkBoxQR = new JCheckBox();

    private static JLabel lblPort = new JLabel("Port:");
    private static JLabel lblAddress = new JLabel("URL:");
    private static JLabel lblScreen = new JLabel("Screen:");
    private static JLabel lblRemoteControl = new JLabel("Remote control:");
    private static JLabel lblMaxUser = new JLabel("Max. users:");
    private static JLabel lblEnableFeedback = new JLabel("Enable feedback:");
    private static JLabel lblLogo = new JLabel(AtreusConstants.ATREUS_LOGO,
            JLabel.LEFT);
    private static JLabel lblWebsite = new JLabel();
    private static JLabel lblShowQrCode = new JLabel("Show QR code:");
    private static JPanel pnlWebsite = new JPanel();
    private static Thread serverThread;
    private static AtreusServer as;
    private static boolean configSuccess;

    private SettingsFrame() {
        super();

        JPanel pnlContainer = new JPanel(new BorderLayout());
        pnlContainer.setBorder(new EmptyBorder(10, 10, 10, 10));

        settingPanel = new SettingsPanel();

        JPanel pnlLogo = new JPanel();

        pnlContainer.setBackground(AtreusConstants.ATREUS_COLOR);

        pnlLogo.setBackground(AtreusConstants.ATREUS_COLOR);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.CENTER;
        GridBagLayout gridbag = new GridBagLayout();
        gridbag.setConstraints(pnlLogo, constraints);
        pnlLogo.setLayout(gridbag);
        lblLogo.setBorder(new EmptyBorder(10, 10, 30, 10));
        pnlLogo.add(lblLogo);
        pnlContainer.add(pnlLogo, BorderLayout.NORTH);

        pnlContainer.add(settingPanel, BorderLayout.CENTER);

        pnlWebsite.setBackground(AtreusConstants.ATREUS_COLOR);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.CENTER;
        gridbag = new GridBagLayout();
        gridbag.setConstraints(pnlWebsite, constraints);
        pnlWebsite.setLayout(gridbag);
        lblWebsite.setBorder(new EmptyBorder(30, 10, 10, 10));
        pnlWebsite.add(lblWebsite);
        pnlContainer.add(pnlWebsite, BorderLayout.SOUTH);

        createComponents();

        getContentPane().add(pnlContainer);

        setIconImage(AtreusConstants.ATREUS_ICON);
        setLocation(AtreusConstants.SETTINGFRAME_POS);
        setPreferredSize(AtreusConstants.SETTINGFRAME_SIZE);
        setMinimumSize(AtreusConstants.SETTINGFRAME_SIZE);
        setSize(AtreusConstants.SETTINGFRAME_SIZE);
        setResizable(false);
        setTitle(AtreusConstants.FRAME_TITLE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();

    }

    public static SettingsFrame getInstance() {
        if (instance == null)
            instance = new SettingsFrame();

        log.info("Open ATREUS window");

        return instance;
    }

    private static void createComponents() {

        // address
        lblAddress.setForeground(Color.WHITE);

        // port
        lblPort.setForeground(Color.WHITE);

        // remote control
        lblRemoteControl.setForeground(Color.WHITE);

        // screen
        lblScreen.setForeground(Color.WHITE);

        // user amount
        lblMaxUser.setForeground(Color.WHITE);
        spinnerUserAmount.setModel(new SpinnerNumberModel(1, 1, 20, 1));

        // feedback
        lblEnableFeedback.setForeground(Color.WHITE);
        checkBoxFB.setBackground(AtreusConstants.ATREUS_COLOR);
        checkBoxFB.setHorizontalAlignment(JLabel.CENTER);

        // QR code
        lblShowQrCode.setForeground(Color.WHITE);
        checkBoxQR.setBackground(AtreusConstants.ATREUS_COLOR);
        checkBoxQR.setHorizontalAlignment(JLabel.CENTER);

        // website
        lblWebsite.setHorizontalAlignment(JLabel.CENTER);
        lblWebsite.setText("<html><a href=\"\" style=\"color:#ffffff;\">"
                + AtreusConstants.ATREUS_URL + "</a></html>");
        lblWebsite.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblWebsite.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(
                            new URI(AtreusConstants.ATREUS_URL));
                } catch (URISyntaxException | IOException ex) {

                }
            }
        });

        btnStart.setEnabled(true);
        btnStart.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {

                btnStart.setEnabled(false);
                checkBoxFB.setEnabled(false);
                checkBoxQR.setEnabled(false);
                textAddress.setEnabled(false);
                textPort.setEnabled(false);
                spinnerUserAmount.setEnabled(false);
                comboRemoteControl.setEnabled(false);
                comboScreen.setEnabled(false);
                btnStop.setEnabled(true);
                startAtreusServer();

            }
        });

        btnStop.setEnabled(false);
        btnStop.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                btnStop.setEnabled(false);
                btnStart.setEnabled(true);
                checkBoxFB.setEnabled(true);
                checkBoxQR.setEnabled(true);
                textAddress.setEnabled(true);
                textPort.setEnabled(true);
                spinnerUserAmount.setEnabled(true);
                comboRemoteControl.setEnabled(true);
                comboScreen.setEnabled(true);

                if (as != null) {

                    stopAtreusServer();
                } else {
                    log.info("Server not started yet!");
                }

            }
        });

        // add to panel
        settingPanel.add(lblAddress);
        settingPanel.add(textAddress);
        settingPanel.add(lblPort);
        settingPanel.add(textPort);
        settingPanel.add(lblRemoteControl);
        settingPanel.add(comboRemoteControl);
        settingPanel.add(lblScreen);
        settingPanel.add(comboScreen);
        settingPanel.add(lblMaxUser);
        settingPanel.add(spinnerUserAmount);
        settingPanel.add(lblEnableFeedback);
        settingPanel.add(checkBoxFB);
        settingPanel.add(lblShowQrCode);
        settingPanel.add(checkBoxQR);
        settingPanel.add(btnStart);
        settingPanel.add(btnStop);
    }

    private static void startAtreusServer() {

        // save selected configuration settings
        final int myPort = AtreusUtils.validPort(textPort.getText());
        final String myIP = AtreusUtils.validURL(textAddress.getText());
        final ScreenRectangle myScreen = comboScreen.getItemAt(comboScreen
                .getSelectedIndex());
        final String myRemoteControl = comboRemoteControl
                .getItemAt(comboRemoteControl.getSelectedIndex());
        int myMaxUser = (int) spinnerUserAmount.getValue();
        boolean isFeedback = checkBoxFB.isSelected();
        boolean isQR = checkBoxQR.isSelected();

        SettingsManager.setPort(myPort);
        SettingsManager.setIP(myIP);
        SettingsManager.setScreenRect(myScreen);
        SettingsManager.setRemoteControl(myRemoteControl);
        SettingsManager.setMaxUser(myMaxUser);
        SettingsManager.setFeedback(isFeedback);
        SettingsManager.setShowqr(isQR);

        // update json config file
        configSuccess = SettingsManager.save();

        if (isQR) {
            qrwindow = QRCodeCreator.getQRCodeWindow(SettingsManager.getURL());
            qrwindow.setVisible(true);
        }

        if (configSuccess) {

            serverThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    // Start server
                    as = AtreusServer.getInstance();

                    // set actionlistener
                    if (myRemoteControl.equals(AtreusConstants.DRIVING_WHEEL)) {
                        as.setActionListener(new DrivingWheelListener());

                    } else if (myRemoteControl.equals(AtreusConstants.FB_DEMO)) {
                        log.error("Only for feedback demo, start FeedbackTest.java");
                    } else if (myRemoteControl.equals(AtreusConstants.GAMEPAD)) {
                        as.setActionListener(new GamepadListener());
                    } else if (SettingsManager.getRemoteControl().equals(
                            AtreusConstants.GAMEPAD_BTN)) {
                        as.setActionListener(new DemoGamepadListener());
                    } else if (SettingsManager.getRemoteControl().equals(
                            AtreusConstants.GAMEPAD_DPAD)) {
                        as.setActionListener(new DemoGamepadListener());
                    } else if (SettingsManager.getRemoteControl().equals(
                            AtreusConstants.GAMEPAD_JOYSTICK)) {
                        as.setActionListener(new DemoGamepadListener());
                    } else if (SettingsManager.getRemoteControl().equals(
                            AtreusConstants.GAMEPAD_TILT)) {
                        as.setActionListener(new DemoGamepadListener());
                    } else if (myRemoteControl
                            .equals(AtreusConstants.MINIVIDEO)) {

                        ScreenStreamer.start(myPort + 1, myScreen);
                        as.setActionListener(new MinivideoListener());
                    } else if (myRemoteControl.equals(AtreusConstants.POINTER)) {
                        as.setActionListener(new PointerListener());
                    } else if (myRemoteControl
                            .equals(AtreusConstants.SMARTLENS)) {
                        PhotoReceiver.start(myPort + 2, myScreen);
                    } else if (myRemoteControl.equals(AtreusConstants.TOUCHPAD)) {
                        as.setActionListener(new TouchpadListener());
                    } else {
                        log.error("No corresponding listener found");
                    }

                    as.start(myIP, myPort, AtreusConstants.HTML_PATH,
                            myRemoteControl);

                }
            });
            serverThread.start();

        } else {
            log.error("Configuration failed");

        }

    }

    private static void stopAtreusServer() {

        as.stop();
        ScreenStreamer.stop();
        PhotoReceiver.stop();

        if (qrwindow != null) {
            qrwindow.setVisible(false);
        }

    }

    public JTextField getTextPort() {
        return textPort;
    }

    public JTextField getTextAddress() {
        return textAddress;
    }

    public JComboBox<ScreenRectangle> getComboScreen() {
        return comboScreen;
    }

    public JComboBox<String> getComboRemoteControl() {
        return comboRemoteControl;
    }

    public JCheckBox getCheckBoxFB() {
        return checkBoxFB;
    }

    public JCheckBox getCheckBoxQR() {
        return checkBoxQR;
    }

    public JSpinner getSpinnerUserAmount() {
        return spinnerUserAmount;
    }

}
