/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.feedback;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import at.ftw.atreus.utils.AtreusConstants;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class FeedbackMessage.
 * 
 * Defines methods for creating JSON messages with feedback data
 * 
 */
public abstract class FeedbackMessage {

    protected Map<String, Object> jsonData = new HashMap<String, Object>();

    private static ObjectMapper mapper = new ObjectMapper();

    public void setBroadcast(boolean broadcast) {
        jsonData.put(AtreusConstants.FEEDBACK_BROADCAST, broadcast);
    }

    public String getMessageString() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            mapper.writeValue(baos, jsonData);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                baos.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return baos.toString();
    }

}
