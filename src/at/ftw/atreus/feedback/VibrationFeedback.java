/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.feedback;

import at.ftw.atreus.utils.AtreusConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class VibrationFeedback.
 */
public class VibrationFeedback extends FeedbackMessage {

    /**
     * Instantiates a new vibration feedback.
     * 
     * @param pattern
     *            the pattern array with vibrations time (ms)
     */
    public VibrationFeedback(long[] pattern) {
        jsonData.put(AtreusConstants.FEEDBACK_TYPE,
                AtreusConstants.FEEDBACK_VIBRATION);
        jsonData.put(AtreusConstants.FEEDBACK_PARAM, pattern);
    }

}
