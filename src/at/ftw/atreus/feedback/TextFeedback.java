/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.feedback;

import at.ftw.atreus.utils.AtreusConstants;

/**
 * The Class TextFeedback.
 * 
 * Class for sending a text message to the smartphone.
 * 
 */
public class TextFeedback extends FeedbackMessage {

    /**
     * Instantiates a new text feedback.
     * 
     * @param text
     *            , the text message
     */
    public TextFeedback(String text) {
        jsonData.put(AtreusConstants.FEEDBACK_TYPE,
                AtreusConstants.FEEDBACK_TEXT);
        jsonData.put(AtreusConstants.FEEDBACK_PARAM, text);
    }

}
