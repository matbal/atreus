/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.feedback;

import at.ftw.atreus.utils.AtreusConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class SoundFeedback.
 * 
 * Class for sending a sound to the smartphone.
 * 
 */
public class SoundFeedback extends FeedbackMessage {

    /**
     * Instantiates a new sound feedback.
     * 
     * @param soundParam
     *            , an array with the path of the sound file and the volume
     */
    public SoundFeedback(String[] soundParam) {
        jsonData.put(AtreusConstants.FEEDBACK_TYPE,
                AtreusConstants.FEEDBACK_SOUND);
        jsonData.put(AtreusConstants.FEEDBACK_PARAM, soundParam);
    }

}
