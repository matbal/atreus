/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/

package at.ftw.atreus.feedback;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;

import at.ftw.atreus.io.IMobileEventListener;
import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.SettingsManager;
import at.ftw.atreus.websocket.AtreusServer;

/**
 * The Class FeedbackTest.
 * 
 * Start feedback demonstrator. feedback = true will be set in
 * atreus_config.json
 * 
 */
public class FeedbackTest {

    private static final Logger log = LogManager.getLogger(FeedbackTest.class);

    public static void main(String[] args) {

        log.info("ATREUS start....");

        final AtreusServer as = AtreusServer.getInstance();

        // enable feedback function
        SettingsManager.setFeedback(true);
        SettingsManager.save();

        as.setEventListener(new IMobileEventListener() {

            @Override
            public void mobileClientDisconnected(String id) {
            }

            @Override
            public void mobileClientConnected(String id, Session session) {
                // send three feedback messages when new client connects:
                // broadcast
                as.sendFeedback(new VibrationFeedback(
                        new long[]{500, 300, 1000}));
                // broadcast
                as.sendFeedback(new SoundFeedback(new String[]{
                        "/ATREUS_mobile/sounds/success.mp3", "0.5"}));
                // targeted
                as.sendFeedback(id, new TextFeedback(
                        "this is a targeted textual feedback for id " + id
                                + "!"));
            }
        });

        as.start(SettingsManager.getIP(), SettingsManager.getPort(),
                AtreusConstants.HTML_PATH, AtreusConstants.FB_DEMO);
    }

}
