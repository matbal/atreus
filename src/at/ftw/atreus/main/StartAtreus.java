/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/

package at.ftw.atreus.main;

import javax.swing.JFrame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ftw.atreus.demos.DemoGamepadListener;
import at.ftw.atreus.io.DrivingWheelListener;
import at.ftw.atreus.io.GamepadListener;
import at.ftw.atreus.io.MinivideoListener;
import at.ftw.atreus.io.PointerListener;
import at.ftw.atreus.io.TouchpadListener;
import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.QRCodeCreator;
import at.ftw.atreus.utils.SettingsManager;
import at.ftw.atreus.websocket.AtreusServer;
import at.ftw.atreus.websocket.PhotoReceiver;
import at.ftw.atreus.websocket.ScreenStreamer;

/**
 * The Class StartAtreus.
 * 
 * Start ATREUS platform with server without GUI Configurations can be done in
 * /data/atreus_config.json
 */
public class StartAtreus {

    private static final Logger log = LogManager.getLogger(StartAtreus.class);

    public static void main(String[] args) {

        log.info("ATREUS start....");

        AtreusServer as = AtreusServer.getInstance();

        // read server address from config
        if (SettingsManager.isShowqr()) {
            JFrame qrwindow = QRCodeCreator.getQRCodeWindow(SettingsManager
                    .getURL());
            qrwindow.setVisible(true);

        }

        // set actionlistener
        if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.DRIVING_WHEEL)) {
            as.setActionListener(new DrivingWheelListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.FB_DEMO)) {
            log.error("Only for feedback demo, start FeedbackTest.java");
        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.GAMEPAD)) {
            as.setActionListener(new GamepadListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.GAMEPAD_BTN)) {
            as.setActionListener(new DemoGamepadListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.GAMEPAD_DPAD)) {
            as.setActionListener(new DemoGamepadListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.GAMEPAD_JOYSTICK)) {
            as.setActionListener(new DemoGamepadListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.GAMEPAD_TILT)) {
            as.setActionListener(new DemoGamepadListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.MINIVIDEO)) {
            ScreenStreamer.start(SettingsManager.getPort() + 1,
                    SettingsManager.getScreenRect());
            as.setActionListener(new MinivideoListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.POINTER)) {
            as.setActionListener(new PointerListener());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.SMARTLENS)) {
            PhotoReceiver.start(SettingsManager.getPort() + 2,
                    SettingsManager.getScreenRect());

        } else if (SettingsManager.getRemoteControl().equals(
                AtreusConstants.TOUCHPAD)) {
            as.setActionListener(new TouchpadListener());
        } else {
            log.error("No corresponding listener found");
        }

        as.start(SettingsManager.getIP(), SettingsManager.getPort(),
                AtreusConstants.HTML_PATH, SettingsManager.getRemoteControl());

    }

}
