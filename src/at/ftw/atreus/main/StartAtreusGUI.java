/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/

package at.ftw.atreus.main;

import java.util.ArrayList;

import at.ftw.atreus.ui.ScreenRectangle;
import at.ftw.atreus.ui.SettingsFrame;
import at.ftw.atreus.utils.AtreusUtils;
import at.ftw.atreus.utils.SettingsManager;

/**
 * The Class StartAtreusGUI.
 * 
 * Start ATREUS platform with server with GUI Configuration file
 * atreus_config.json can be modified with the GUI
 * 
 */
public class StartAtreusGUI {

    private static String[] allRemoteControls;

    public static void main(String[] args) {

        // get all screens
        ArrayList<ScreenRectangle> screens = AtreusUtils.getScreenResolutions();

        ScreenRectangle defaultScreen = new ScreenRectangle(
                SettingsManager.getScreenX(), SettingsManager.getScreenY(),
                SettingsManager.getScreenWidth(),
                SettingsManager.getScreenHeight());

        // get all remote control files
        ArrayList<String> remoteControlList = new ArrayList<String>();
        AtreusUtils.setRemoteControls(remoteControlList);

        allRemoteControls = new String[remoteControlList.size()];
        allRemoteControls = remoteControlList.toArray(allRemoteControls);

        // start GUI
        try {

            SettingsFrame settingsFrame = SettingsFrame.getInstance();

            // fill with default data from setting file
            settingsFrame.getTextPort().setText(
                    String.valueOf(SettingsManager.getPort()));
            settingsFrame.getTextAddress().setText(SettingsManager.getIP());
            for (ScreenRectangle screenItem : screens) {
                settingsFrame.getComboScreen().addItem(screenItem);

            }
            settingsFrame.getComboScreen().setSelectedItem(defaultScreen);

            for (String rcItem : allRemoteControls) {
                settingsFrame.getComboRemoteControl().addItem(rcItem);
            }
            settingsFrame.getComboRemoteControl().setSelectedItem(
                    SettingsManager.getRemoteControl());

            settingsFrame.getSpinnerUserAmount().setValue(
                    SettingsManager.getMaxUser());
            settingsFrame.getCheckBoxFB().setSelected(
                    SettingsManager.isFeedback());
            settingsFrame.getCheckBoxQR().setSelected(SettingsManager.isShowqr());
            settingsFrame.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
