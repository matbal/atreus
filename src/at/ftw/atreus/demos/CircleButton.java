/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * The Class CircleButton.
 * 
 * Defines the circular button for the pointing test
 */
public class CircleButton extends Button {

    private int radius;

    public CircleButton(Point center, int radius, Color color) {
        super(center);
        this.radius = radius;
        this.color = color;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        g.fillOval(center.x - radius, center.y - radius, radius * 2, radius * 2);
    }

    @Override
    public boolean contains(int x, int y) {
        int dx = center.x - x;
        int dy = center.y - y;
        return (dx * dx + dy * dy <= radius * radius);
    }

}
