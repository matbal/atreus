/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * The Class DemoSettings.
 * 
 * Constant values for the pointing test demonstrator
 * 
 */
public class DemoSettings {

    public static int USER_ID = 0;
    public static int BLOCK_ID = 0;
    public static int GAMEPAD_ID = 0;
    public static int PHASE_ID = 0;
    public static boolean TEST_RUNNING = false;

    public static final String DEMO_HTML = "gamepad_btn.html";
    public static final String BACKUP_DIR = ".";
    public static final String BTN_LEFT = "cross_left";
    public static final String BTN_RIGHT = "cross_right";
    public static final String BTN_UP = "cross_up";
    public static final String BTN_DOWN = "cross_down";
    public static final String BTN_A = "buttonA";
    public static final String BTN_B = "buttonB";

    public static final Dimension STUDYFRAME_SIZE = new Dimension(1920, 1080);
    public static final Point STUDYFRAME_POS = new Point(0, 0);
    public static final Point STUDYCONSOLE_POS = new Point(1920, 0);
    public static final int[] DISTANCES = {150, 320};
    public static final int[] SIZES = {40, 80};
    public static final int[] ORIENTATIONS = {0, 45, 90, 135, 180, 225, 270,
            315};
    public static final boolean TARGET_SHUFFLED = true;

    public static final Color COLOR_BG = Color.decode("#D5EBDE");
    public static final Color COLOR_START = Color.decode("#29794C");
    public static final Color COLOR_RUNNING = Color.RED;
    public static final Color COLOR_TARGET = Color.decode("#497387");

    public static final String SUCCESSFILE = "data" + File.separatorChar
            + "success.wav";
    public static final String FAILFILE = "data" + File.separatorChar
            + "fail.wav";
    public static BufferedImage CURSOR_IMG = null;

    static {
        try {
            CURSOR_IMG = ImageIO.read(new File("data" + File.separatorChar
                    + "cursor_circle.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
