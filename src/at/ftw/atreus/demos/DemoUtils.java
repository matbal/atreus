/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * The Class DemoUtils.
 * 
 * Helper methods for pointing test demonstrator: Playing audio files and create
 * log-files
 * 
 */
public class DemoUtils {

    public static Clip successClip;
    public static Clip failClip;

    static {

        try {
            successClip = AudioSystem.getClip();
            AudioInputStream successais = AudioSystem
                    .getAudioInputStream(new File(DemoSettings.SUCCESSFILE));
            successClip.open(successais);

            failClip = AudioSystem.getClip();
            AudioInputStream failais = AudioSystem
                    .getAudioInputStream(new File(DemoSettings.FAILFILE));
            failClip.open(failais);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void play(Clip c) {
        c.stop();
        c.setFramePosition(0);
        c.start();
    }

    public static void backupLogs() {
        File backupDir = new File(DemoSettings.BACKUP_DIR);
        backupDir.mkdir();

        File taskFileBackup = new File(backupDir, System.currentTimeMillis()
                + "_studytasks.log");
        File taskFile = new File("logs", "studytasks.log");
        try {
            Files.copy(taskFile.toPath(), taskFileBackup.toPath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        File keysFileBackup = new File(backupDir, System.currentTimeMillis()
                + "_studykeys.log");
        File keysFile = new File("logs", "studykeys.log");
        try {
            Files.copy(keysFile.toPath(), keysFileBackup.toPath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
