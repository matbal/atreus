/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import at.ftw.atreus.feedback.SoundFeedback;
import at.ftw.atreus.feedback.TextFeedback;
import at.ftw.atreus.feedback.VibrationFeedback;
import at.ftw.atreus.io.IMobileActionListener;
import at.ftw.atreus.io.MobileData;
import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.websocket.AtreusServer;

/**
 * The Class DemoFrame.
 * 
 * Defines the frame for the pointing test demonstrator
 * 
 */
public class DemoFrame extends JFrame {

    private static DemoFrame instance;
    private AtreusServer as;
    private static final long serialVersionUID = 2823980945023785082L;
    private StudyPanel studypanel;

    public StudyPanel getStudyPanel() {
        return studypanel;
    }

    public void setAtreusServer(AtreusServer as) {
        this.as = as;
    }

    public static DemoFrame getInstance() {
        if (instance == null)
            instance = new DemoFrame();
        return instance;
    }

    private DemoFrame() {
        super();

        getContentPane().setLayout(new BorderLayout());
        studypanel = new StudyPanel();
        getContentPane().add(studypanel, BorderLayout.CENTER);

        setLocation(DemoSettings.STUDYFRAME_POS);

        setPreferredSize(DemoSettings.STUDYFRAME_SIZE);
        setMinimumSize(DemoSettings.STUDYFRAME_SIZE);
        setSize(DemoSettings.STUDYFRAME_SIZE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();

        studypanel.resetTargets();
    }

    /**
     * The Class StudyPanel.
     * 
     * Defines the panel for the DemoFrame and the feedback action
     * 
     */
    class StudyPanel extends JPanel
            implements
                KeyListener,
                MouseListener,
                IMobileActionListener {

        private static final long serialVersionUID = -1702521216492954467L;
        private Font startFont = new Font("Arial", Font.PLAIN, 32);
        private Font thanksFont = new Font("Arial", Font.BOLD, 64);
        private Color thanksColor = Color.decode("#FFFFFF");
        private String thanksText = "Test beendet. Danke!";
        private Color startColor = Color.decode("#FFFFFF");
        private String startText = "Start";

        private boolean trialRunning = false;
        private Button startButton;
        private Vector<Button> targetButton = new Vector<>();
        private int trialcounter = 0;

        private Point cursorPos = new Point();
        private boolean up, down, left, right;
        private int initacc = 4;
        private float acc = initacc;
        private int fps = 50;

        private long taskStart;
        private Timer updateTimer;

        private Point getTargetPoint(int distance, double angle) {
            int cx = getSize().width / 2;
            int cy = getSize().height / 2;
            int x = (int) (cx + Math.cos(Math.toRadians(angle)) * distance);
            int y = (int) (cy + Math.sin(Math.toRadians(angle)) * distance);
            return new Point(x, y);
        }

        public void resetTargets() {
            targetButton.clear();

            for (int distance : DemoSettings.DISTANCES) {
                for (int size : DemoSettings.SIZES) {
                    for (int orientation : DemoSettings.ORIENTATIONS) {
                        Button target = new CircleButton(getTargetPoint(
                                distance, orientation), size,
                                DemoSettings.COLOR_TARGET);
                        target.setParams(distance, size, orientation);
                        targetButton.add(target);
                    }
                }
            }

            if (DemoSettings.TARGET_SHUFFLED)
                Collections.shuffle(targetButton);

            up = false;
            down = false;
            left = false;
            right = false;
            acc = initacc;

            trialcounter = 0;
            trialRunning = false;

            centerCursor();
        }

        public StudyPanel() {
            super();

            setFocusable(true);
            requestFocusInWindow();

            startButton = new RectButton(new Point(), 200, 100,
                    DemoSettings.COLOR_START);
            startButton.setText(startText, startFont, startColor);

            setBackground(DemoSettings.COLOR_BG);
            addMouseListener(this);
            addKeyListener(this);

            updateTimer = new javax.swing.Timer(1000 / fps,
                    new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            int delta = (int) acc;

                            // compensate for diagonal movement
                            if (up || down)
                                if (left || right)
                                    delta = (int) Math.round(acc / Math.sqrt(2));

                            if (up
                                    && cursorPos.y - delta > DemoSettings.CURSOR_IMG
                                            .getHeight() / 2) {
                                cursorPos.y = cursorPos.y - delta;
                            }
                            if (down
                                    && cursorPos.y + delta < getHeight()
                                            - DemoSettings.CURSOR_IMG
                                                    .getHeight() / 2) {
                                cursorPos.y = cursorPos.y + delta;
                            }
                            if (left
                                    && cursorPos.x - delta > DemoSettings.CURSOR_IMG
                                            .getWidth() / 2) {
                                cursorPos.x = cursorPos.x - delta;
                            }
                            if (right
                                    && cursorPos.x + delta < getWidth()
                                            - DemoSettings.CURSOR_IMG
                                                    .getWidth() / 2) {
                                cursorPos.x = cursorPos.x + delta;
                            }

                            repaint();
                        }
                    });
            updateTimer.start();
        }

        public void setCursorPos(int x, int y) {
            cursorPos.x = x;
            cursorPos.y = y;
            repaint();
        }

        public void centerCursor() {
            setCursorPos(getWidth() / 2, getHeight() / 2);
            repaint();
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);

            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);

            if (!trialRunning) {
                if (trialcounter < targetButton.size()) {
                    startButton.setColor(DemoSettings.COLOR_START);
                    startButton.setCenter(getWidth() / 2, getHeight() / 2);
                    startButton.draw(g2);
                } else {
                    g2.setColor(new Color(0, 0, 0, 80));
                    g2.fillRect(0, 0, getWidth(), getHeight());

                    FontMetrics fm = g2.getFontMetrics(thanksFont);
                    Rectangle2D border = fm.getStringBounds(thanksText, g2);
                    g2.setColor(thanksColor);
                    g2.setFont(thanksFont);
                    g2.drawString(thanksText,
                            (int) (getWidth() - border.getWidth()) / 2,
                            (int) (getHeight() - border.getHeight()) / 2);

                    // send thanks text
                    as.sendFeedback(new TextFeedback(thanksText));
                }
            } else {
                if (trialcounter < targetButton.size()) {

                    targetButton.get(trialcounter).draw(g2);
                }
            }

            if (trialcounter < targetButton.size())
                g2.drawImage(DemoSettings.CURSOR_IMG, cursorPos.x
                        - DemoSettings.CURSOR_IMG.getWidth() / 2, cursorPos.y
                        - DemoSettings.CURSOR_IMG.getHeight() / 2, null);
        }

        public void clicked(int x, int y) {
            long duration = System.currentTimeMillis() - taskStart;

            if (trialRunning) {
                if (targetButton.get(trialcounter).contains(x, y)) {

                    DemoUtils.play(DemoUtils.successClip);
                    // send audio (not in every browser?)
                    as.sendFeedback(new SoundFeedback(new String[]{
                            "/ATREUS_mobile/sounds/success.mp3", "0.5"}));
                    as.sendFeedback(new VibrationFeedback(new long[]{500, 300}));

                    up = false;
                    down = false;
                    left = false;
                    right = false;
                    acc = initacc;

                    trialRunning = false;
                    trialcounter++;

                    startButton.setText(startText, startFont, startColor);

                    centerCursor();
                } else {
                    DemoUtils.play(DemoUtils.failClip);
                    // send audio
                    as.sendFeedback(new SoundFeedback(new String[]{
                            "/ATREUS_mobile/sounds/fail.wav", "0.5"}));

                }
            } else {
                if (startButton.contains(x, y)) {
                    trialRunning = true;

                    repaint();
                    taskStart = System.currentTimeMillis();
                }
            }

        }

        public void setUp(boolean up) {
            this.up = up;
        }

        public void setDown(boolean down) {
            this.down = down;
        }

        public void setLeft(boolean left) {
            this.left = left;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            int c = e.getKeyCode();
            if (c == KeyEvent.VK_A) {
                clicked(cursorPos.x, cursorPos.y);
            }
            if (c == KeyEvent.VK_UP) {
                up = true;
            } else if (c == KeyEvent.VK_DOWN) {
                down = true;
            } else if (c == KeyEvent.VK_LEFT) {
                left = true;
            } else if (c == KeyEvent.VK_RIGHT) {
                right = true;
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            initacc = 5;
            int c = e.getKeyCode();
            if (c == KeyEvent.VK_UP) {
                up = false;
            } else if (c == KeyEvent.VK_DOWN) {
                down = false;
            } else if (c == KeyEvent.VK_LEFT) {
                left = false;
            } else if (c == KeyEvent.VK_RIGHT) {
                right = false;
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            cursorPos.x = e.getX();
            cursorPos.y = e.getY();
            clicked(cursorPos.x, cursorPos.y);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseEntered(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseExited(MouseEvent e) {
            // TODO Auto-generated method stub
        }

        @Override
        public void triggerAction(MobileData mobiledata) {

            if (mobiledata != null) {

                // exploit acc interaction for passing sensitivity parameter
                if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_ACC) {
                    if (mobiledata.getSensorData()[0].equals("slow")) {
                        acc = initacc / 2;
                    }
                    if (mobiledata.getSensorData()[0].equals("fast")) {
                        acc = initacc;
                    }
                }

                Button currentTarget = null;
                if (targetButton != null && trialcounter < targetButton.size())
                    currentTarget = targetButton.get(trialcounter);

                if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_BUTTON) {

                    int actionType = mobiledata.getAction();
                    String elementId = mobiledata.getElementId();

                    if ("buttonA".equals(elementId)) {
                        if (actionType == AtreusConstants.ATREUS_DOWN)
                            clicked(cursorPos.x, cursorPos.y);
                    }
                    if ("buttonB".equals(elementId)) {
                        if (actionType == AtreusConstants.ATREUS_DOWN)
                            clicked(cursorPos.x, cursorPos.y);
                    }
                    if ("cross_left".equals(elementId)) {
                        if (actionType == AtreusConstants.ATREUS_DOWN) {
                            setLeft(true);
                        } else if (actionType == AtreusConstants.ATREUS_UP) {
                            setLeft(false);
                        }
                    }
                    if ("cross_right".equals(elementId)) {
                        if (actionType == AtreusConstants.ATREUS_DOWN) {
                            setRight(true);
                        } else if (actionType == AtreusConstants.ATREUS_UP) {
                            setRight(false);
                        }
                    }
                    if ("cross_up".equals(elementId)) {
                        if (actionType == AtreusConstants.ATREUS_DOWN) {
                            setUp(true);
                        } else if (actionType == AtreusConstants.ATREUS_UP) {
                            setUp(false);
                        }
                    }
                    if ("cross_down".equals(elementId)) {
                        if (actionType == AtreusConstants.ATREUS_DOWN) {
                            setDown(true);
                        } else if (actionType == AtreusConstants.ATREUS_UP) {
                            setDown(false);
                        }
                    }

                }
            }
        }
    }

}
