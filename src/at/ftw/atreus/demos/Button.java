/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

/**
 * The Class Button.
 * 
 * Abstract class for button styling
 */
public abstract class Button {

    protected Point center;
    protected Color color;
    protected String text;
    protected Font font;
    protected Color textcolor;
    protected int distance, size, orientation;

    public int getDistance() {
        return distance;
    }

    public int getSize() {
        return size;
    }

    public int getOrientation() {
        return orientation;
    }

    public Button(Point center) {
        this.center = center;
    }

    public void setCenter(int x, int y) {
        this.center = new Point(x, y);
    }

    public Point getCenter() {
        return center;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setText(String text, Font font, Color textcolor) {
        this.text = text;
        this.font = font;
        this.textcolor = textcolor;
    }

    public void setParams(int distance, int size, int orientation) {
        this.distance = distance;
        this.size = size;
        this.orientation = orientation;
    }

    public abstract void draw(Graphics g);

    public abstract boolean contains(int x, int y);
}