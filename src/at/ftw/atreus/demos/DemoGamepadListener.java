/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import at.ftw.atreus.io.IMobileActionListener;
import at.ftw.atreus.io.MobileData;
import at.ftw.atreus.utils.AtreusConstants;

/**
 * Listener for the pointing test demonstrator Inputs received from
 * gamepad_btn.html
 * 
 * The listener interface for receiving Gamepad events. The class that is
 * interested in processing the DemoGamepad event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addDemoGamepadListener<code> method. When
 * the demoGamepad event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see DemoGamepadEvent
 */
public class DemoGamepadListener implements IMobileActionListener {

    private static Robot robot;
    private Hashtable<String, Integer>[] keymapping;

    public DemoGamepadListener() {
        // create robot instance
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        keymapping = new Hashtable[3];

        keymapping[0] = new Hashtable<String, Integer>();
        keymapping[0].put(DemoSettings.BTN_LEFT, new Integer(KeyEvent.VK_A));
        keymapping[0].put(DemoSettings.BTN_RIGHT, new Integer(KeyEvent.VK_D));
        keymapping[0].put(DemoSettings.BTN_UP, new Integer(KeyEvent.VK_W));
        keymapping[0].put(DemoSettings.BTN_DOWN, new Integer(KeyEvent.VK_S));

        keymapping[1] = new Hashtable<String, Integer>();
        keymapping[1].put(DemoSettings.BTN_LEFT, new Integer(KeyEvent.VK_A));
        keymapping[1].put(DemoSettings.BTN_RIGHT, new Integer(KeyEvent.VK_D));
        keymapping[1].put(DemoSettings.BTN_UP, new Integer(KeyEvent.VK_W));
        keymapping[1].put(DemoSettings.BTN_DOWN, new Integer(KeyEvent.VK_S));
        keymapping[1].put(DemoSettings.BTN_A, new Integer(KeyEvent.VK_Z));
        keymapping[1].put(DemoSettings.BTN_B, new Integer(KeyEvent.VK_X));

        keymapping[2] = new Hashtable<String, Integer>();
        keymapping[2].put(DemoSettings.BTN_LEFT, new Integer(KeyEvent.VK_LEFT));
        keymapping[2].put(DemoSettings.BTN_RIGHT,
                new Integer(KeyEvent.VK_RIGHT));
        keymapping[2].put(DemoSettings.BTN_UP, new Integer(KeyEvent.VK_UP));
        keymapping[2].put(DemoSettings.BTN_DOWN, new Integer(KeyEvent.VK_DOWN));
        keymapping[2].put(DemoSettings.BTN_A, new Integer(KeyEvent.VK_A));
        keymapping[2].put(DemoSettings.BTN_B, new Integer(KeyEvent.VK_X));
    }

    @Override
    public void triggerAction(MobileData mobiledata) {

        if (mobiledata != null) {
            if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_BUTTON) {

                int actionType = mobiledata.getAction();
                String elementId = mobiledata.getElementId();

                // mapping available?
                if (keymapping[2] != null) {

                    // key defined?
                    if (keymapping[2].containsKey(elementId)) {

                        int keycode = keymapping[2].get(elementId).intValue();

                        // simulate keyboard action
                        if (actionType == AtreusConstants.ATREUS_DOWN) {
                            robot.keyPress(keycode);
                        } else if (actionType == AtreusConstants.ATREUS_UP) {
                            robot.keyRelease(keycode);
                        }
                    }
                }
            }
        }
    }

}
