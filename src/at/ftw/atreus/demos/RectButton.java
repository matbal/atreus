/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;

/**
 * The Class RectButton.
 * 
 * Defines the rectangular start button
 */
public class RectButton extends Button {

    private int width, height, left, top;

    public RectButton(Point center, int width, int height, Color color) {
        super(center);
        this.width = width;
        this.height = height;
        this.left = center.x - width / 2;
        this.top = center.y - height / 2;
        this.color = color;
    }

    @Override
    public void setCenter(int x, int y) {
        super.setCenter(x, y);
        this.left = center.x - width / 2;
        this.top = center.y - height / 2;
    };

    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        g.fillRect(left, top, width, height);

        if (text != null && font != null && textcolor != null) {
            FontMetrics fm = g.getFontMetrics(font);
            Rectangle bounds = fm.getStringBounds(text, g).getBounds();

            FontRenderContext renderContext = ((Graphics2D) g)
                    .getFontRenderContext();
            GlyphVector glyphVector = font.createGlyphVector(renderContext,
                    text);
            Rectangle visualBounds = glyphVector.getVisualBounds().getBounds();
            int textx = center.x - bounds.width / 2;
            int texty = center.y - visualBounds.height / 2 - visualBounds.y;

            g.setColor(textcolor);
            g.setFont(font);
            g.drawString(text, textx, texty);
        }
    }

    @Override
    public boolean contains(int x, int y) {
        return (x >= left && x <= left + width && y >= top && y <= top + height);
    }

}
