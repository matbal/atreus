/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.demos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.SettingsManager;
import at.ftw.atreus.websocket.AtreusServer;

/**
 * The Class StartDemo.
 * 
 * Start the pointing test demonstration. Default remote control used:
 * gamepad_btn.html
 */
public class StartDemo {

    private static AtreusServer as;

    public static void main(String[] args) {

        final DemoFrame studyFrame = DemoFrame.getInstance();
        new Thread() {
            @Override
            public void run() {
                as = AtreusServer.getInstance();
                as.setActionListener(new DemoGamepadListener());
                studyFrame.setAtreusServer(as);
                SettingsManager.setFeedback(true);
                as.start(SettingsManager.getIP(), SettingsManager.getPort(),
                        AtreusConstants.HTML_PATH, DemoSettings.DEMO_HTML);
            };
        }.start();

        studyFrame.getStudyPanel().resetTargets();
        studyFrame.setVisible(true);

    }
}

class TestConsole extends JFrame {

    private static AtreusServer as;

    private static Process testApp;
    private static DemoGamepadListener robotListener = new DemoGamepadListener();

    private static final long serialVersionUID = 6152121520561836544L;
    private JFormattedTextField textUser;
    private JFormattedTextField textBlock;
    private JButton btnStart = new JButton("Start");
    private JRadioButton[] btnGamepad = new JRadioButton[4];
    private JRadioButton[] btnPhase = new JRadioButton[4];
    private ParameterUpdateListener parameterListener = new ParameterUpdateListener();

    private int stopwatchSeconds = 0;
    private Timer stopwatch;
    private JLabel stopwatchLabel = new JLabel();

    private void setControlsEnabled(boolean enabled) {
        textUser.setEnabled(enabled);
        textBlock.setEnabled(enabled);
        for (JRadioButton btn : btnGamepad) {
            btn.setEnabled(enabled);
        }
        for (JRadioButton btn : btnPhase) {
            btn.setEnabled(enabled);
        }
    }

    public TestConsole() {
        super("[ATREUS] Test Console");
        setResizable(false);
        setLocation(DemoSettings.STUDYCONSOLE_POS);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        NumberFormat f = NumberFormat.getNumberInstance();
        f.setMaximumIntegerDigits(2);
        textUser = new JFormattedTextField(f);
        textUser.setText("1");
        textBlock = new JFormattedTextField(f);
        textBlock.setText("1");

        btnGamepad[0] = new JRadioButton("1  Tasten");
        btnGamepad[0].setSelected(true);
        btnGamepad[1] = new JRadioButton("2  Steuerkreuz");
        btnGamepad[2] = new JRadioButton("3  Joystick");
        btnGamepad[3] = new JRadioButton("4  Neigung");

        btnPhase[0] = new JRadioButton("1a Pointing - Training");
        btnPhase[0].setSelected(true);
        btnPhase[1] = new JRadioButton("1b Pointing - Test");
        btnPhase[2] = new JRadioButton("2a Gaming - Pacman");
        btnPhase[3] = new JRadioButton("2b Gaming - Super Mario");

        ButtonGroup groupGamepad = new ButtonGroup();
        for (JRadioButton btn : btnGamepad) {
            groupGamepad.add(btn);
            btn.addActionListener(parameterListener);
        }
        ButtonGroup groupPhase = new ButtonGroup();
        for (JRadioButton btn : btnPhase) {
            groupPhase.add(btn);
            btn.addActionListener(parameterListener);
        }

        JPanel controls = new JPanel();
        controls.setLayout(new GridLayout(0, 2));
        controls.add(new JLabel("User #"));
        controls.add(textUser);
        controls.add(new JLabel("Block #"));
        controls.add(textBlock);
        controls.add(new JLabel("Phase #"));
        controls.add(btnPhase[0]);
        controls.add(new JLabel());
        controls.add(btnPhase[1]);
        controls.add(new JLabel());
        controls.add(btnPhase[2]);
        controls.add(new JLabel());
        controls.add(btnPhase[3]);
        controls.add(new JLabel("Gamepad #"));
        controls.add(btnGamepad[0]);
        controls.add(new JLabel());
        controls.add(btnGamepad[1]);
        controls.add(new JLabel());
        controls.add(btnGamepad[2]);
        controls.add(new JLabel());
        controls.add(btnGamepad[3]);

        stopwatch = new javax.swing.Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stopwatchSeconds++;

                int mins = stopwatchSeconds / 60;
                int secs = stopwatchSeconds % 60;

                stopwatchLabel.setText(mins + ":" + (secs < 10 ? "0" : "")
                        + secs);
            }
        });

        btnStart.setBackground(Color.GREEN);
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (DemoSettings.TEST_RUNNING) {
                    // ignore button presses if test not running
                    stopwatch.stop();

                    as.setActionListener(null);

                    setControlsEnabled(true);

                    btnStart.setBackground(Color.GREEN);
                    btnStart.setText("Start");

                    if (DemoFrame.getInstance().isVisible())
                        DemoFrame.getInstance().setVisible(false);
                    else if (testApp != null) {
                        testApp.destroy();
                        testApp = null;
                    }
                } else {
                    setControlsEnabled(false);

                    updateParameters();

                    btnStart.setBackground(Color.RED);
                    btnStart.setText("Stop");

                    stopwatchSeconds = 0;
                    stopwatchLabel.setText("0:00");
                    stopwatch.restart();

                    if (DemoSettings.PHASE_ID <= 1) {
                        DemoFrame studyFrame = DemoFrame.getInstance();
                        as.setActionListener(studyFrame.getStudyPanel());
                        studyFrame.getStudyPanel().resetTargets();
                        studyFrame.setVisible(true);
                    } else {
                        as.setActionListener(robotListener);

                    }
                }
                DemoSettings.TEST_RUNNING = !DemoSettings.TEST_RUNNING;
            }
        });

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(controls, BorderLayout.CENTER);

        JPanel bottom = new JPanel();
        bottom.setLayout(new GridLayout(0, 1));
        stopwatchLabel.setText("0:00");
        stopwatchLabel.setMaximumSize(new Dimension(300, 50));
        stopwatchLabel.setFont(new Font("Arial", Font.PLAIN, 32));
        stopwatchLabel.setHorizontalAlignment(SwingConstants.CENTER);
        bottom.add(stopwatchLabel);
        bottom.add(btnStart);

        getContentPane().add(bottom, BorderLayout.SOUTH);

        pack();

        new Thread() {
            @Override
            public void run() {
                as = AtreusServer.getInstance();
                as.start(SettingsManager.getIP(), SettingsManager.getPort(),
                        AtreusConstants.HTML_PATH, "gamepad.html");
            };
        }.start();

    }

    private void updateParameters() {
        DemoSettings.USER_ID = Integer.parseInt(textUser.getText());
        DemoSettings.BLOCK_ID = Integer.parseInt(textBlock.getText());
        for (int i = 0; i < btnGamepad.length; i++) {
            if (btnGamepad[i].isSelected())
                DemoSettings.GAMEPAD_ID = i;
        }
        for (int i = 0; i < btnPhase.length; i++) {
            if (btnPhase[i].isSelected())
                DemoSettings.PHASE_ID = i;
        }
    }

    class ParameterUpdateListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            updateParameters();
        }
    }
}
