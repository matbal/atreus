/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.websocket;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import at.ftw.atreus.io.IMobileActionListener;
import at.ftw.atreus.io.MobileData;
import at.ftw.atreus.utils.SettingsManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class AtreusServerSocket.
 * 
 * Handles the Jetty WebSocket connection
 * 
 */
@WebSocket(maxTextMessageSize = 64 * 1024)
public class AtreusServerSocket {

    private static final Logger log = LogManager
            .getLogger(AtreusServerSocket.class);
    private final CountDownLatch closeLatch;
    private String clientID;

    private Session session;

    public AtreusServerSocket() {
        this.closeLatch = new CountDownLatch(1); // 1 thread should wait until
                                                 // other operations are
                                                 // finished

    }

    public boolean awaitClose(int duration, TimeUnit unit)
            throws InterruptedException {
        return this.closeLatch.await(duration, unit);

    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {

        if (AtreusServer.getInstance().getAllSessions()
                .containsKey(this.clientID)) {

            // call mobileEventListener
            if (AtreusServer.getInstance().getEventListener() != null)
                AtreusServer.getInstance().getEventListener()
                        .mobileClientDisconnected(this.clientID);

            // remove client
            AtreusServer.getInstance().getAllSessions().remove(this.clientID);

            if (AtreusServer.getInstance().getAllClients().isEmpty()) {
                log.info("No clients connected");
            } else {
                log.info("Last clients: "
                        + AtreusServer.getInstance().getAllClients());
            }
        }

        // close connection
        log.info("Connection with client " + clientID + " closed: "
                + statusCode + " | " + reason);

        session.close(StatusCode.NORMAL, "Finished");
        session = null;
        this.closeLatch.countDown();
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        // store reference to session
        this.session = session;

        // create unique client identifier
        this.clientID = session.getRemoteAddress().getHostString() + ":"
                + session.getRemoteAddress().getPort();
        log.info(this.clientID + " tries to connect...");

        // send screen resolution
        if (session != null) {

            String reply = null;

            if (SettingsManager.getMaxUser() > 0
                    && AtreusServer.getInstance().getAllSessions().size() > SettingsManager
                            .getMaxUser()) {

                reply = "{\"responsecode\": 1, \"message\": \"Connection failed, max user reached\"}";

            } else {

                reply = "{\"responsecode\": 0, "
                        + "\"message\": \"Connection successful\", "
                        + "\"screenx\": " + SettingsManager.getScreenX() + ", "
                        + "\"screeny\": " + SettingsManager.getScreenY() + ", "
                        + "\"screenwidth\": "
                        + SettingsManager.getScreenWidth() + ", "
                        + "\"screenheight\": "
                        + SettingsManager.getScreenHeight() + ", "
                        + "\"feedback\": " + SettingsManager.isFeedback() + "}";

                // add to list of connected clients
                AtreusServer.getInstance().getAllSessions()
                        .put(this.clientID, session);

                // call mobileEventListener
                if (AtreusServer.getInstance().getEventListener() != null)
                    AtreusServer.getInstance().getEventListener()
                            .mobileClientConnected(this.clientID, this.session);

            }

            try {
                Future<Void> fut;
                fut = session.getRemote().sendStringByFuture(reply);
                fut.get(1, TimeUnit.SECONDS);

                log.info("Replied to " + this.clientID + ": " + reply);
            } catch (Throwable t) {

                t.printStackTrace();
            }
            // close connection if maxuser reached
            if (SettingsManager.getMaxUser() > 0
                    && AtreusServer.getInstance().getAllSessions().size() > SettingsManager
                            .getMaxUser())
                session.close();

        }

    }

    @OnWebSocketMessage
    public void onMessage(String msg) {

        if (msg.startsWith("{")) {

            MobileData data = null;
            ObjectMapper mobileMapper = new ObjectMapper();
            try {
                data = mobileMapper.readValue(msg, MobileData.class);
                data.setClientId(this.clientID);

            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            log.info("Received command from client " + data.getClientId()
                    + ": " + data.toString());

            IMobileActionListener actionListener = AtreusServer.getInstance()
                    .getActionListener();
            if (actionListener != null)
                actionListener.triggerAction(data);
        }

    }

    @OnWebSocketError
    public void onError(Throwable t) {
        log.error("WebSocketError: " + t.getMessage());
    }

}
