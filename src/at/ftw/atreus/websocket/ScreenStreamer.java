/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.websocket;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import at.ftw.atreus.utils.ScreenCapture;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

// TODO: Auto-generated Javadoc
/**
 * The Class ScreenStreamer.
 * 
 * Handles and sends the screenshots used in minivideo.html
 */
public class ScreenStreamer {
    private static final Logger log = LogManager
            .getLogger(ScreenStreamer.class);
    private static StreamingThread streamingThread = null;
    private static Vector<Session> clients = new Vector<>();

    public static int getNrClients() {
        return clients.size();
    }

    public static void addClient(Session session) {
        log.info("new client connected!");
        clients.add(session);
    }

    public static void removeClient(Session session) {
        log.info("client disconnected!");
        clients.removeElement(session);
    }

    /**
     * The Class StreamingThread.
     */
    static class StreamingThread extends Thread {
        private int port;
        private Server streamingServer;
        private boolean running = true;
        private Rectangle captureRect;

        public StreamingThread(int port, Rectangle captureRect) {
            this.port = port;
            this.captureRect = captureRect;
        }

        public void cancel() {

            running = false;

            try {
                streamingServer.stop();
                streamingServer.destroy();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            streamingServer = new Server(port);

            // create/register Handler for WebsocketServer
            WebSocketHandler wsHandler = new WebSocketHandler() {
                @Override
                public void configure(WebSocketServletFactory wsServlet) {

                    wsServlet.register(ScreenStreamerConnect.class);
                }
            };

            HandlerList allHandlers = new HandlerList();
            allHandlers.setHandlers(new Handler[]{wsHandler});

            streamingServer.setHandler(allHandlers);
            try {
                streamingServer.start();
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            while (running) {
                if (!clients.isEmpty()) {
                    byte[] screenshotjpg = ScreenCapture.getJPGScreenshot(
                            captureRect, 0.5f);
                    String screenshotbase64 = Base64.encode(screenshotjpg);

                    for (Session s : clients)
                        try {
                            s.getRemote().sendString(screenshotbase64);
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }
    }

    public static void start(int port, Rectangle captureRect) {

        // already running?
        if (streamingThread != null)
            return;

        streamingThread = new StreamingThread(port, captureRect);
        streamingThread.start();

        log.info("ATREUS video streamer (" + (int) captureRect.getX() + ", "
                + (int) captureRect.getY() + ", "
                + (int) captureRect.getWidth() + ", "
                + (int) captureRect.getHeight() + ") started...");
    }

    public static void stop() {
        if (streamingThread != null) {
            clients.clear();
            streamingThread.cancel();
            streamingThread = null;

        }
    }

}