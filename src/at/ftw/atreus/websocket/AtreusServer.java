/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.websocket;

import java.io.IOException;
import java.net.BindException;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import at.ftw.atreus.feedback.FeedbackMessage;
import at.ftw.atreus.io.IMobileActionListener;
import at.ftw.atreus.io.IMobileEventListener;
import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.AtreusUtils;

/**
 * The Class AtreusServer.
 * 
 * Creates the Jetty WebSocket server
 * 
 */
public class AtreusServer {

    private static final Logger log = LogManager.getLogger(AtreusServer.class);
    private static AtreusServer serverInstance = null;
    private Server server;
    private IMobileActionListener mobileActionListener = null;
    private IMobileEventListener mobileEventListener = null;
    private HashMap<String, Session> allSessions = new HashMap<String, Session>();
    private ScreenStreamer screenCaptureThread = null;

    public static AtreusServer getInstance() {
        if (serverInstance == null) {
            serverInstance = new AtreusServer();
            log.info("ATREUS http server started...");
        }

        return serverInstance;
    }

    public void start(String ip, int port, String resourceBase,
            String welcomePage) {

        server = new Server(port);

        AtreusUtils.initWebsocketSettings(ip, port, AtreusConstants.JS_FILE);

        // create/register Handler for WebsocketServer
        WebSocketHandler wsHandler = new WebSocketHandler() {

            @Override
            public void configure(WebSocketServletFactory wsServlet) {
                // TODO Auto-generated method stub
                wsServlet.register(AtreusServerSocket.class);

            }

        };

        // create handler for html files
        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setWelcomeFiles(new String[]{welcomePage});
        resource_handler.setResourceBase(resourceBase);

        HandlerList allHandlers = new HandlerList();
        allHandlers.setHandlers(new Handler[]{wsHandler, resource_handler});

        server.setHandler(allHandlers);

        try {
            server.start();
        } catch (BindException e) {

            log.error("Problem binding to port " + port + " : "
                    + e.getMessage());
            JOptionPane.showMessageDialog(null, "Problem binding to port "
                    + port + ". Please try another port!", "Server error",
                    JOptionPane.ERROR_MESSAGE);

            if (server != null) {
                try {
                    this.stop();
                } catch (Exception ex) {

                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            server.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
        }

    }

    public void stop() {

        if (server != null) {
            try {

                this.server.stop();

                this.server.destroy();
                this.getAllSessions().clear();
                this.getAllClients().clear();

                log.info("ATREUS server stopped...");

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public void sendFeedback(FeedbackMessage feedback) {
        if (feedback == null)
            return;

        feedback.setBroadcast(true);

        for (Session s : allSessions.values()) {
            try {
                s.getRemote().sendString(feedback.getMessageString());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void sendFeedback(String client, FeedbackMessage feedback) {
        if (client == null || feedback == null)
            return;

        feedback.setBroadcast(false);

        if (allSessions.containsKey(client)) {
            try {
                allSessions.get(client).getRemote()
                        .sendString(feedback.getMessageString());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public IMobileActionListener getActionListener() {
        return mobileActionListener;
    }

    public void setActionListener(IMobileActionListener mobileActionListener) {
        this.mobileActionListener = mobileActionListener;
    }

    public IMobileEventListener getEventListener() {
        return mobileEventListener;
    }

    public void setEventListener(IMobileEventListener mobileEventListener) {
        this.mobileEventListener = mobileEventListener;
    }

    public Set<String> getAllClients() {
        return allSessions.keySet();
    }

    public HashMap<String, Session> getAllSessions() {
        return allSessions;
    }

}
