/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.websocket;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

/**
 * The Class PhotoReceiver.
 * 
 * Handles the connection for receiving images from smartlens.html
 * 
 */
public class PhotoReceiver {
    private static ReceivingThread photoReceivingThread = null;
    private static Robot robot = null;

    public static void click(int x, int y) {
        robot.mouseMove(x, y);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    static {
        // create robot instance
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * The Class ReceivingThread.
     */
    static class ReceivingThread extends Thread {
        private int port;
        private Server receivingserver;
        private boolean running = true;

        public ReceivingThread(int port, Rectangle captureRect) {
            this.port = port;

        }

        public void cancel() {
            running = false;
            try {
                receivingserver.stop();
                receivingserver.destroy();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            receivingserver = new Server(port);

            // create/register Handler for WebsocketServer
            WebSocketHandler wsHandler = new WebSocketHandler() {
                @Override
                public void configure(WebSocketServletFactory wsServlet) {

                    wsServlet.register(PhotoReceiverConnect.class);
                }
            };

            HandlerList allHandlers = new HandlerList();
            allHandlers.setHandlers(new Handler[]{wsHandler});

            receivingserver.setHandler(allHandlers);
            try {
                receivingserver.start();
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            while (running) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public static void start(int port, Rectangle captureRect) {

        // already running?
        if (photoReceivingThread != null)
            return;

        photoReceivingThread = new ReceivingThread(port, captureRect);
        photoReceivingThread.start();
    }

    public static void stop() {
        if (photoReceivingThread != null) {
            photoReceivingThread.cancel();
            photoReceivingThread = null;
        }
    }

}