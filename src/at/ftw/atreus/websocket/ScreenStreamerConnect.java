/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.websocket;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import at.ftw.atreus.utils.SettingsManager;

/**
 * The Class ScreenStreamerConnect.
 * 
 * Handles the second connection for streaming screen
 * 
 */
@WebSocket(maxTextMessageSize = 64 * 1024)
public class ScreenStreamerConnect {

    private static final Logger log = LogManager
            .getLogger(ScreenStreamerConnect.class);
    private final CountDownLatch closeLatch;
    private Session session;

    public ScreenStreamerConnect() {
        this.closeLatch = new CountDownLatch(1); // 1 thread should wait until
                                                 // other operations are
                                                 // finished
    }

    public boolean awaitClose(int duration, TimeUnit unit)
            throws InterruptedException {
        return this.closeLatch.await(duration, unit);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        ScreenStreamer.removeClient(this.session);
        session.close(StatusCode.NORMAL, "Finished");
        session = null;
        this.closeLatch.countDown();
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;

        if (ScreenStreamer.getNrClients() < SettingsManager.getMaxUser())
            ScreenStreamer.addClient(this.session);
        else
            this.session.close();
    }

}
