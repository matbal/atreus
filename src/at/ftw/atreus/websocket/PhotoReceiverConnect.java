/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.websocket;

import georegression.fitting.homography.ModelManagerHomography2D_F64;
import georegression.struct.homo.Homography2D_F64;
import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point2D_I32;
import georegression.transform.homo.HomographyPointOps_F64;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ddogleg.fitting.modelset.ModelMatcher;
import org.ddogleg.fitting.modelset.ransac.Ransac;
import org.ddogleg.struct.FastQueue;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketFrame;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.api.extensions.Frame;

import at.ftw.atreus.utils.ScreenCapture;
import at.ftw.atreus.utils.SettingsManager;
import boofcv.abst.feature.associate.AssociateDescription;
import boofcv.abst.feature.associate.ScoreAssociation;
import boofcv.abst.feature.detdesc.DetectDescribePoint;
import boofcv.abst.feature.detect.interest.ConfigFastHessian;
import boofcv.alg.distort.ImageDistort;
import boofcv.alg.distort.PixelTransformHomography_F32;
import boofcv.alg.distort.impl.DistortSupport;
import boofcv.alg.feature.UtilFeature;
import boofcv.alg.interpolate.impl.ImplBilinearPixel_F32;
import boofcv.alg.sfm.robust.DistanceHomographySq;
import boofcv.alg.sfm.robust.GenerateHomographyLinear;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.factory.feature.associate.FactoryAssociation;
import boofcv.factory.feature.detdesc.FactoryDetectDescribe;
import boofcv.gui.image.ShowImages;
import boofcv.struct.feature.AssociatedIndex;
import boofcv.struct.feature.SurfFeature;
import boofcv.struct.feature.TupleDesc;
import boofcv.struct.geo.AssociatedPair;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageSingleBand;
import boofcv.struct.image.MultiSpectral;

/**
 * The Class PhotoReceiverConnect.
 * 
 * Handles incoming image file and calculates homography wrt to current screen
 * content. Major parts based on BoofCV's image stitching example, available at
 * http://boofcv.org/index.php?title=Example_Image_Stitching
 * 
 */
@WebSocket(maxBinaryMessageSize = 163840000)
public class PhotoReceiverConnect {

    private static final Logger log = LogManager
            .getLogger(PhotoReceiverConnect.class);
    private final CountDownLatch closeLatch;
    private Session session;

    private static DetectDescribePoint detDesc;
    private static ScoreAssociation<SurfFeature> scorer;
    private static AssociateDescription<SurfFeature> associate;
    private ByteBuffer tempBuffer;
    private byte[] tempSize;

    static {
        // Detect using the standard SURF feature descriptor and describer
        detDesc = FactoryDetectDescribe.surfStable(new ConfigFastHessian(1, 2,
                200, 1, 9, 4, 4), null, null, ImageFloat32.class);
        scorer = FactoryAssociation.scoreEuclidean(SurfFeature.class, true);
        associate = FactoryAssociation.greedy(scorer, 2, true);
    }

    public PhotoReceiverConnect() {
        this.closeLatch = new CountDownLatch(1);

    }

    public boolean awaitClose(int duration, TimeUnit unit)
            throws InterruptedException {
        return this.closeLatch.await(duration, unit);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        session.close(StatusCode.NORMAL, "Finished");
        session = null;
        this.closeLatch.countDown();
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        session.getPolicy().setInputBufferSize(30000);

        this.session = session;
    }

    @OnWebSocketMessage
    public void onMessage(String msg) {
        String[] deviceInfo = msg.split(",");

        System.out.println(msg);

        int imageBufSize = Integer.parseInt(deviceInfo[0]);
        int x = Integer.parseInt(deviceInfo[1]);
        int y = Integer.parseInt(deviceInfo[2]);

        if (imageBufSize > 0) {
            tempSize = new byte[imageBufSize];
            tempBuffer = ByteBuffer.wrap(tempSize);
        } else {
            byte[] buffer = tempBuffer.array();

            long start = System.currentTimeMillis();

            BufferedImage imageScreen, imagePhoto = null;
            try {
                imagePhoto = ImageIO.read(new ByteArrayInputStream(buffer));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            imageScreen = ScreenCapture.getScreenshot(SettingsManager
                    .getScreenRect());

            Homography2D_F64 homography = calculateMatching(imageScreen,
                    imagePhoto, ImageFloat32.class, x, y);

            // map the submitted point to screen coordinates and trigger mouse
            // click
            Point2D_I32 p = renderPoint(x, y, homography.invert(null));
            int posx = SettingsManager.getScreenX() + p.x;
            int posy = SettingsManager.getScreenY() + p.y;
            PhotoReceiver.click(posx, posy);

            log.info("Image matching done in "
                    + ((System.currentTimeMillis() - start)) / 1000f
                    + " seconds");
        }
    }

    @OnWebSocketMessage
    public void onMessage(Session session, byte[] buffer, int offset, int length) {
        if (tempSize.length > 0) {
            tempBuffer.put(buffer);
        }
    }

    @OnWebSocketFrame
    public void onFrame(Frame frame) {
        if (frame.getType() == Frame.Type.TEXT) {
            // log.info("Size " + frame.getPayload().get(0));
        } else {
            int length = frame.getPayloadLength();
            log.info("framesize " + length + " | onFrame: " + frame);
        }
    }

    @OnWebSocketError
    public void onWSError(Session session, Throwable cause) {
        log.info("error: " + cause.getLocalizedMessage());
        log.info("error2: " + cause.getStackTrace().toString());
        log.info("inputbuffersize: " + session.getPolicy().getInputBufferSize());
        log.info("maxBinMsgBufferSize: "
                + session.getPolicy().getMaxBinaryMessageBufferSize());
        log.info("maxBinMsgSize: "
                + session.getPolicy().getMaxBinaryMessageSize());
    }

    public static <T extends ImageSingleBand, FD extends TupleDesc> Homography2D_F64 computeTransform(
            T imageA, T imageB, DetectDescribePoint<T, FD> detDesc,
            AssociateDescription<FD> associate,
            ModelMatcher<Homography2D_F64, AssociatedPair> modelMatcher) {
        // get the length of the description
        java.util.List<Point2D_F64> pointsA = new ArrayList<Point2D_F64>();
        FastQueue<FD> descA = UtilFeature.createQueue(detDesc, 100);
        java.util.List<Point2D_F64> pointsB = new ArrayList<Point2D_F64>();
        FastQueue<FD> descB = UtilFeature.createQueue(detDesc, 100);

        // extract feature locations and descriptions from each image
        describeImage(imageA, detDesc, pointsA, descA);
        describeImage(imageB, detDesc, pointsB, descB);

        // Associate features between the two images
        associate.setSource(descA);
        associate.setDestination(descB);
        associate.associate();

        // create a list of AssociatedPairs that tell the model matcher how a
        // feature moved
        FastQueue<AssociatedIndex> matches = associate.getMatches();
        java.util.List<AssociatedPair> pairs = new ArrayList<AssociatedPair>();

        for (int i = 0; i < matches.size(); i++) {
            AssociatedIndex match = matches.get(i);

            Point2D_F64 a = pointsA.get(match.src);
            Point2D_F64 b = pointsB.get(match.dst);

            pairs.add(new AssociatedPair(a, b, false));
        }

        // find the best fit model to describe the change between these images
        if (!modelMatcher.process(pairs))
            throw new RuntimeException("Model Matcher failed!");

        // return the found image transform
        return modelMatcher.getModelParameters().copy();
    }

    private static <T extends ImageSingleBand, FD extends TupleDesc> void describeImage(
            T image, DetectDescribePoint<T, FD> detDesc,
            java.util.List<Point2D_F64> points, FastQueue<FD> listDescs) {
        detDesc.detect(image);

        listDescs.reset();
        for (int i = 0; i < detDesc.getNumberOfFeatures(); i++) {
            points.add(detDesc.getLocation(i).copy());
            listDescs.grow().setTo(detDesc.getDescription(i));
        }
    }

    /**
     * Given two input images create and display an image where the two have
     * been overlayed on top of each other.
     */
    public static <T extends ImageSingleBand> Homography2D_F64 calculateMatching(
            BufferedImage imageA, BufferedImage imageB, Class<T> imageType,
            int x, int y) {
        T inputA = ConvertBufferedImage.convertFromSingle(imageA, null,
                imageType);
        T inputB = ConvertBufferedImage.convertFromSingle(imageB, null,
                imageType);

        // fit the images using a homography. This works well for rotations and
        // distant objects.
        ModelManagerHomography2D_F64 manager = new ModelManagerHomography2D_F64();
        GenerateHomographyLinear modelFitter = new GenerateHomographyLinear(
                true);
        DistanceHomographySq distance = new DistanceHomographySq();

        ModelMatcher<Homography2D_F64, AssociatedPair> modelMatcher = new Ransac<Homography2D_F64, AssociatedPair>(
                123, manager, modelFitter, distance, 60, 9);

        Homography2D_F64 H = computeTransform(inputA, inputB, detDesc,
                associate, modelMatcher);

        // renderStitching(imageA,imageB,H, x, y);

        return H;
    }

    /**
     * Renders and displays the stitched together images
     */
    public static void renderStitching(BufferedImage imageA,
            BufferedImage imageB, Homography2D_F64 fromAtoB, int x, int y) {
        // specify size of output image
        double scale = 0.2;
        int outputWidth = imageA.getWidth();
        int outputHeight = imageA.getHeight();

        // Convert into a BoofCV color format
        MultiSpectral<ImageFloat32> colorA = ConvertBufferedImage
                .convertFromMulti(imageA, null, true, ImageFloat32.class);
        MultiSpectral<ImageFloat32> colorB = ConvertBufferedImage
                .convertFromMulti(imageB, null, true, ImageFloat32.class);

        // Where the output images are rendered into
        MultiSpectral<ImageFloat32> work = new MultiSpectral<ImageFloat32>(
                ImageFloat32.class, outputWidth, outputHeight, 3);

        // Adjust the transform so that the whole image can appear inside of it
        Homography2D_F64 fromAToWork = new Homography2D_F64(scale, 0,
                colorA.width / 4, 0, scale, colorA.height / 4, 0, 0, 1);
        Homography2D_F64 fromWorkToA = fromAToWork.invert(null);

        // Used to render the results onto an image
        PixelTransformHomography_F32 model = new PixelTransformHomography_F32();
        ImageDistort<MultiSpectral<ImageFloat32>> distort = DistortSupport
                .createDistortMS(ImageFloat32.class, model,
                        new ImplBilinearPixel_F32(), null);

        // Render first image
        model.set(fromWorkToA);
        distort.apply(colorA, work);

        // Render second image
        Homography2D_F64 fromWorkToB = fromWorkToA.concat(fromAtoB, null);
        model.set(fromWorkToB);
        distort.apply(colorB, work);

        // Convert the rendered image into a BufferedImage
        BufferedImage output = new BufferedImage(work.width, work.height,
                imageA.getType());
        ConvertBufferedImage.convertTo(work, output, true);

        Graphics2D g2 = output.createGraphics();

        // draw lines around the distorted image to make it easier to see
        Homography2D_F64 fromBtoWork = fromWorkToB.invert(null);
        Point2D_I32 corners[] = new Point2D_I32[4];
        corners[0] = renderPoint(0, 0, fromBtoWork);
        corners[1] = renderPoint(colorB.width, 0, fromBtoWork);
        corners[2] = renderPoint(colorB.width, colorB.height, fromBtoWork);
        corners[3] = renderPoint(0, colorB.height, fromBtoWork);

        g2.setColor(Color.ORANGE);
        g2.setStroke(new java.awt.BasicStroke(4));
        g2.drawLine(corners[0].x, corners[0].y, corners[1].x, corners[1].y);
        g2.drawLine(corners[1].x, corners[1].y, corners[2].x, corners[2].y);
        g2.drawLine(corners[2].x, corners[2].y, corners[3].x, corners[3].y);
        g2.drawLine(corners[3].x, corners[3].y, corners[0].x, corners[0].y);

        Point2D_I32 touch = renderPoint(x, y, fromBtoWork);
        g2.drawOval(touch.x - 3, touch.y - 3, 6, 6);

        ShowImages.showWindow(output, "Stitched Images");
    }

    private static Point2D_I32 renderPoint(int x0, int y0,
            Homography2D_F64 fromBtoWork) {
        Point2D_F64 result = new Point2D_F64();
        HomographyPointOps_F64.transform(fromBtoWork, new Point2D_F64(x0, y0),
                result);
        return new Point2D_I32((int) result.x, (int) result.y);
    }

}
