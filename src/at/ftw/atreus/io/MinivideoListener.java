/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;

import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.SettingsManager;

/**
 * Listener for minivideo.html
 * 
 * Handles touch events and triggers corresponding keyboard actions.
 * 
 * The listener interface for receiving minivideo events. The class that is
 * interested in processing a minivideo event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addMinivideoListener<code> method. When
 * the minivideo event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see MinivideoEvent
 */
public class MinivideoListener implements IMobileActionListener {

    private Robot robot;

    public MinivideoListener() {
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void triggerAction(MobileData mobiledata) {
        if (mobiledata != null) {
            if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_TOUCH) {

                if (mobiledata.getAction() == AtreusConstants.ATREUS_MOVE) {
                    int x = SettingsManager.getScreenX()
                            + (int) mobiledata.getCoords().x;
                    int y = SettingsManager.getScreenY()
                            + (int) mobiledata.getCoords().y;
                    robot.mouseMove(x, y);
                }
                if (mobiledata.getAction() == AtreusConstants.ATREUS_DOWN) {
                    robot.mousePress(InputEvent.BUTTON1_MASK);
                    robot.mouseRelease(InputEvent.BUTTON1_MASK);
                }
            }
        }
    }

}