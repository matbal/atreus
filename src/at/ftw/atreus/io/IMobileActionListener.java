/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

/**
 * Handles input events from mobile device and triggers an action
 * 
 * The listener interface for receiving IMobileAction events. The class that is
 * interested in processing a IMobileAction event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addIMobileActionListener<code> method. When
 * the IMobileAction event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see IMobileActionEvent
 */
public interface IMobileActionListener {
    public void triggerAction(MobileData mobiledata);
}
