/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.SettingsManager;

/**
 * Listener for using touchpad.html
 * 
 * Handles touch events (swipe, scroll and zoom), text events and geolocation
 * events and triggers corresponding keyboard actions.
 * 
 * The listener interface for receiving touchpad events. The class that is
 * interested in processing a touchpad event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addTouchpadListener<code> method. When
 * the touchpad event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see TouchpadEvent
 */
public class TouchpadListener implements IMobileActionListener {

    private final static String mappingFileName = "data/touchpad.map";
    private static final Logger log = LogManager
            .getLogger(TouchpadListener.class);
    private HashMap<String, Integer> buttonMouseMapping, buttonKeyMapping;
    private Robot robot;
    private int currentPosX, currentPosY;
    private float SCROLL_FACTOR = -0.05f;

    public TouchpadListener() {

        // read button/key mapping from file
        buttonMouseMapping = new HashMap<>();
        buttonKeyMapping = new HashMap<>();
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(new File(mappingFileName)));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        // run through properties, check and add to hashmap
        for (Object p : props.keySet()) {
            String propkey = p.toString();
            String propvalue = props.getProperty(propkey);

            Integer mousecode = null;
            Integer keycode = null;

            try {
                // check whether numeric mousecode is specified
                mousecode = new Integer(Integer.parseInt(propvalue));

            } catch (NumberFormatException e1) {
                // if not try to interpret value as keyevent constant
                try {
                    mousecode = (Integer) InputEvent.class.getDeclaredField(
                            propvalue).get(Integer.class);

                } catch (Exception e2) {

                    try {
                        // check whether numeric keycode is specified

                        keycode = new Integer(Integer.parseInt(propvalue));

                    } catch (NumberFormatException e3) {
                        // if not try to interpret value as keyevent constant
                        try {

                            keycode = (Integer) KeyEvent.class
                                    .getDeclaredField(propvalue).get(
                                            Integer.class);

                        } catch (Exception e4) {

                        }
                    }
                }
            }

            // if parsing was successful add to mapping table
            if (mousecode != null) {
                buttonMouseMapping.put(propkey, mousecode);
            } else if (keycode != null) {
                buttonKeyMapping.put(propkey, keycode);
            }

        }

        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void triggerAction(MobileData mobiledata) {
        // TODO Auto-generated method stub
        if (mobiledata != null) {

            int actionType = mobiledata.getAction();
            String elementId = mobiledata.getElementId();

            if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_BUTTON) {

                if (buttonMouseMapping.containsKey(elementId)) {

                    int mousecode = buttonMouseMapping.get(elementId)
                            .intValue();

                    // simulate mouse actions

                    if (actionType == AtreusConstants.ATREUS_DOWN) {
                        robot.mousePress(mousecode);

                    } else if (actionType == AtreusConstants.ATREUS_UP) {
                        robot.mouseRelease(mousecode);

                    } else if (actionType == AtreusConstants.ATREUS_LONGDOWN) {
                        int longPresscode = buttonMouseMapping.get(
                                String.valueOf(actionType)).intValue();
                        robot.mousePress(longPresscode);
                        robot.mouseRelease(longPresscode);
                    }
                }

            } else if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_TOUCH) {

                int keycode = 0;
                String mapkey = String.valueOf(actionType);
                if (buttonKeyMapping.containsKey(mapkey)) {
                    keycode = buttonKeyMapping.get(mapkey).intValue();
                }

                if (actionType == AtreusConstants.ATREUS_SCROLL_UP
                        || actionType == AtreusConstants.ATREUS_SCROLL_DOWN) {

                    int verticalDiff = (int) (Double.parseDouble(mobiledata
                            .getSensorData()[1]) * SCROLL_FACTOR);

                    robot.mouseWheel(verticalDiff);

                } else if (actionType == AtreusConstants.ATREUS_MOVE) {

                    int x = (int) mobiledata.getCoords().x;
                    int y = (int) mobiledata.getCoords().y;
                    /*currentPosX = x
                            * (SettingsManager.getScreenWidth() / Integer
                                    .parseInt(mobiledata.getSensorData()[1]));
                    currentPosY = y
                            * (SettingsManager.getScreenHeight() / Integer
                                    .parseInt(mobiledata.getSensorData()[2]));;*/
                    Point mousepos = MouseInfo.getPointerInfo().getLocation();
                    robot.mouseMove(mousepos.x + x, mousepos.y + y);

                } else {

                    robot.keyPress(keycode);
                    robot.keyRelease(keycode);

                }

            } else if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_GEO) {
                // geolocation demo
                double latitude = mobiledata.getCoords().x;
                double longitude = mobiledata.getCoords().y;
                double altitude = mobiledata.getCoords().z;
                log.info("latitude: " + latitude + " | longitude: " + longitude
                        + " | altitude: " + altitude);

            } else if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_TEXT) {
                // Demo text from keyboard
                String keycode = mobiledata.getSensorData()[0];
                String keyChar = mobiledata.getSensorData()[1];

                log.info("keycode: " + keycode + " | keycharacter: " + keyChar);
            }
        }

    }

}