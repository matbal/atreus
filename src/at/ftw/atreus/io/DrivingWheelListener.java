/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import at.ftw.atreus.utils.AtreusConstants;

/**
 * Listener for using drivingwheel.html
 * 
 * Handles orientation events and triggers corresponding keyboard actions.
 * 
 * The listener interface for receiving drivingWheel events. The class that is
 * interested in processing a drivingWheel event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addDrivingWheelListener<code> method. When
 * the drivingWheel event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see DrivingWheelEvent
 */
public class DrivingWheelListener implements IMobileActionListener {

    private final static String mappingFileName = "data/drivingwheel.map";

    private Robot robot;
    private HashMap<String, Integer> buttonKeyMapping;
    private double degDirection;
    private double degBack;
    private boolean touched;

    // key constants defined in configuration map
    private String CROSS_UP = "cross_up";
    private String CROSS_DOWN = "cross_down";
    private String CROSS_LEFT = "cross_left";
    private String CROSS_RIGHT = "cross_right";
    private String LEFT_VALUE = "left_value";
    private String RIGHT_VALUE = "right_value";
    private String BACK_VALUE = "back_value";

    public DrivingWheelListener() {

        // read button/key mapping from file
        buttonKeyMapping = new HashMap<>();
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(new File(mappingFileName)));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        // run through properties, check and add to hashmap
        for (Object p : props.keySet()) {
            String propkey = p.toString();
            String propvalue = props.getProperty(propkey);

            Integer keycode = null;
            try {
                // check whether numeric keycode is specified
                keycode = new Integer(Integer.parseInt(propvalue));
            } catch (NumberFormatException e1) {
                // if not try to interpret value as keyevent constant
                try {
                    keycode = (Integer) KeyEvent.class.getDeclaredField(
                            propvalue).get(Integer.class);
                } catch (Exception e2) {
                }
            }

            // if parsing was successful add to mapping table
            if (keycode != null)
                buttonKeyMapping.put(propkey, keycode);
        }

        // create robot instance
        try {
            robot = new Robot();

        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void triggerAction(MobileData mobiledata) {
        // TODO Auto-generated method stub

        if (mobiledata != null) {

            if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_ORIENT) {

                degDirection = mobiledata.getCoords().y;
                degBack = mobiledata.getCoords().x;
                touched = Boolean.parseBoolean(mobiledata.getSensorData()[0]);

                controlDirection(degDirection);

                if (touched) {
                    robot.keyPress(buttonKeyMapping.get(CROSS_UP).intValue());

                } else {
                    robot.keyRelease(buttonKeyMapping.get(CROSS_UP).intValue());

                    if (degBack > buttonKeyMapping.get(BACK_VALUE).intValue()) {

                        robot.keyPress(buttonKeyMapping.get(CROSS_DOWN)
                                .intValue());
                    } else {
                        robot.keyRelease(buttonKeyMapping.get(CROSS_DOWN)
                                .intValue());
                    }

                }

            }

        }

    }

    private void controlDirection(double deg) {
        // left, neg.
        if (deg < buttonKeyMapping.get(LEFT_VALUE).intValue() && deg < 0) { //

            robot.keyPress(buttonKeyMapping.get(CROSS_LEFT).intValue());

            // right, pos
        } else if (deg > buttonKeyMapping.get(RIGHT_VALUE).intValue()) {

            robot.keyPress(buttonKeyMapping.get(CROSS_RIGHT).intValue());

            // straight
        } else {

            robot.keyRelease(buttonKeyMapping.get(CROSS_RIGHT).intValue());
            robot.keyRelease(buttonKeyMapping.get(CROSS_LEFT).intValue());

        }
    }

}
