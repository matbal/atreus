/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import at.ftw.atreus.utils.AtreusConstants;

/**
 * Listener for gamepad.html
 * 
 * Handles touch events and triggers corresponding keyboard actions.
 * 
 * The listener interface for receiving gamepad events. The class that is
 * interested in processing a gamepad event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addGamepadListener<code> method. When
 * the gamepad event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see GamepadEvent
 */
public class GamepadListener implements IMobileActionListener {

    private final static String mappingFileName = "data/gamepad.map";

    private Robot robot;
    private HashMap<String, Integer> buttonKeyMapping;

    public GamepadListener() {

        // read button/key mapping from file
        buttonKeyMapping = new HashMap<>();
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(new File(mappingFileName)));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        // run through properties, check and add to hashmap
        for (Object p : props.keySet()) {
            String propkey = p.toString();
            String propvalue = props.getProperty(propkey);

            Integer keycode = null;
            try {
                // check whether numeric keycode is specified
                keycode = new Integer(Integer.parseInt(propvalue));
            } catch (NumberFormatException e1) {
                // if not try to interpret value as keyevent constant
                try {
                    keycode = (Integer) KeyEvent.class.getDeclaredField(
                            propvalue).get(Integer.class);
                } catch (Exception e2) {
                }
            }

            // if parsing was successful add to mapping table
            if (keycode != null)
                buttonKeyMapping.put(propkey, keycode);
        }

        // create robot instance
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void triggerAction(MobileData mobiledata) {

        if (mobiledata != null) {
            if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_BUTTON) {

                int actionType = mobiledata.getAction();
                String elementId = mobiledata.getElementId();

                // do we have a mapping for this gamepad button?
                if (buttonKeyMapping.containsKey(elementId)) {

                    int keycode = buttonKeyMapping.get(elementId).intValue();

                    // simulate keyboard action
                    if (actionType == AtreusConstants.ATREUS_DOWN) {
                        robot.keyPress(keycode);
                    } else if (actionType == AtreusConstants.ATREUS_UP) {
                        robot.keyRelease(keycode);
                    }
                }
            }
        }
    }

}
