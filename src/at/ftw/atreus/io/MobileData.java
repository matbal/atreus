/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

import java.util.Arrays;

import javax.vecmath.Point3d;

/**
 * The Class MobileData.
 * 
 * Contains mobile data received from ATREUS mobile platform
 */
public class MobileData {

    private int interactionType; // button, touch, etc.
    private int action; // down, move, up, etc.
    private String elementId;
    private Point3d coords; // x, y,z (e.g. touch)
    private String[] sensorData;
    private String clientId;

    @Override
    public String toString() {
        return interactionType + ";" + action + ";" + elementId + ";"
                + (coords != null ? coords.toString() : "null") + ";"
                + (sensorData != null ? Arrays.toString(sensorData) : "null");

    };

    public int getInteractionType() {
        return interactionType;
    }

    public void setInteractionType(int interactionType) {
        this.interactionType = interactionType;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String id) {
        this.elementId = id;
    }

    public Point3d getCoords() {
        return coords;
    }

    public void setCoords(Point3d coords) {
        this.coords = coords;
    }

    public String[] getSensorData() {
        return sensorData;
    }

    public void setSensorData(String[] sensorData) {
        this.sensorData = sensorData;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientID) {
        this.clientId = clientID;
    }

}
