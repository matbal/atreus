/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

import org.eclipse.jetty.websocket.api.Session;

/**
 * Handles client websocket connection
 * 
 * The listener interface for receiving IMobileEvent events. The class that is
 * interested in processing a IMobileEvent event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addIMobileEventListener<code> method. When
 * the IMobileEvent event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see IMobileEventEvent
 */
public interface IMobileEventListener {
    public void mobileClientConnected(String id, Session session);

    public void mobileClientDisconnected(String id);
}
