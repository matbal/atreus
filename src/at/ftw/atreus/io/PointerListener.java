/********************************************************
 * ATREUS platform for web-based remote controls
 * ======================================================
 * 
 * Copyright (C) 2014, FTW
 * License: http://www.gnu.org/licenses/gpl.html
 * @author http://atreus.ftw.at
 * @version: 1.0, 2014/10/31
 *
 ********************************************************/
package at.ftw.atreus.io;

import java.awt.AWTException;
import java.awt.Robot;

import at.ftw.atreus.utils.AtreusConstants;
import at.ftw.atreus.utils.SettingsManager;

/**
 * Listener for using pointer.html
 * 
 * Handles rotation events and triggers corresponding mouse actions.
 * 
 * The listener interface for receiving pointer events. The class that is
 * interested in processing a pointer event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addPointerListener<code> method. When
 * the pointer event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see PointerEvent
 */
public class PointerListener implements IMobileActionListener {

    private Robot robot;

    public PointerListener() {
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void triggerAction(MobileData mobiledata) {
        if (mobiledata != null) {
            if (mobiledata.getInteractionType() == AtreusConstants.INTERACTION_TOUCH) {
                int x = SettingsManager.getScreenX()
                        + (int) mobiledata.getCoords().x;
                int y = SettingsManager.getScreenY()
                        + (int) mobiledata.getCoords().y;
                robot.mouseMove(x, y);
            }
        }
    }

}
